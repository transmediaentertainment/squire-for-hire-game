﻿using UnityEngine;
using System.Collections;
using System;

public class InputController : MonoBehaviour {
	private Item m_CarryingItem = null;
	public Camera m_MovingUICam;
	public Camera m_ThirdPersonCamera;
	private int m_ItemAndThrowMask;
	private int m_BackingMask;
	private int m_TempThrowAndTrashMask;
	private int m_KnightMask;
	private int m_SquireMask;

	private Vector3 m_BackingOffset = new Vector3(0f, 0f, -0.05f);
	private Vector3 m_Offset;

	public event Action<Item> ItemThrownFromSquireToKnight;
	public event Action<Item> ItemPickedUp;
	public event Action ItemPutDown;
	public event Action ThrowIconClicked;
	public event Action SquireClicked;

	void Awake() {
		m_ItemAndThrowMask = LayerMask.GetMask ("Item", "Throw");
		m_BackingMask = LayerMask.GetMask ("Backing");
		m_TempThrowAndTrashMask = LayerMask.GetMask ("Trash", "TempStore", "Throw", "Inventory");
		m_KnightMask = LayerMask.GetMask ("Knight");
		m_SquireMask = LayerMask.GetMask ("Squire");
	}

	public void CleanUp() {
		if (m_CarryingItem != null) {
			m_CarryingItem.CleanUp();
			Destroy(m_CarryingItem.gameObject);
		}
	}

	public void ForceReplaceCarryingItem(Item item) {
		if (m_CarryingItem != null) {
			m_CarryingItem.RevertToLastState();
		}
		m_Offset = Vector3.zero;
		m_CarryingItem = item;
	}

	private Ray GetMovingUIRay()
	{
		return m_MovingUICam.ScreenPointToRay(Input.mousePosition);
	}

	private Ray GetThirdPersonRay() 
	{
		return m_ThirdPersonCamera.ScreenPointToRay (Input.mousePosition);
	}

	void Update() {
		if (m_CarryingItem != null) {
			if(Input.GetKeyDown(KeyCode.Z)) {
				m_CarryingItem.RotateCounterClockwise();
			}
			if(Input.GetKeyDown(KeyCode.X)) {
				m_CarryingItem.RotateClockwise();
			}
			if(Input.GetMouseButtonDown(1)) {
				m_CarryingItem.RotateClockwise();
			}
			RaycastHit hit;
			if(Physics.Raycast(GetMovingUIRay(), out hit, 100f, m_BackingMask)) {
				m_CarryingItem.transform.position = hit.point + m_BackingOffset + m_Offset;
			}
			if(Input.GetMouseButtonUp(0)) {
				RaycastHit knightHit;
				RaycastHit tempThrowTrashHit;

				if(Physics.Raycast(GetMovingUIRay(), out tempThrowTrashHit, 100f, m_TempThrowAndTrashMask)) {
					if(tempThrowTrashHit.transform.gameObject.layer == LayerMask.NameToLayer("Trash")) {
						m_CarryingItem.Trashed();
					} else if(tempThrowTrashHit.transform.gameObject.layer == LayerMask.NameToLayer("TempStore")){
						// Temp Store
						if(!m_CarryingItem.PutItemInTempStore()) {
							m_CarryingItem.DroppedInLowerSection();
						}
					} else if(tempThrowTrashHit.transform.gameObject.layer == LayerMask.NameToLayer("Throw")) {
						if(m_CarryingItem.m_IsThrowable) {
							if(ItemThrownFromSquireToKnight != null) {
								ItemThrownFromSquireToKnight(m_CarryingItem);
								m_CarryingItem.Thrown();
							}
						} else {
							m_CarryingItem.DroppedInLowerSection();
						}
					} else {
						m_CarryingItem.DroppedInLowerSection();
					}
				} else if(Physics.Raycast(GetThirdPersonRay(), out knightHit, 1000f, m_KnightMask)) {
					if(m_CarryingItem.m_IsThrowable) {
						if(ItemThrownFromSquireToKnight != null) {
							ItemThrownFromSquireToKnight(m_CarryingItem);
							m_CarryingItem.Thrown();
						}
					} else {
						m_CarryingItem.DroppedInLowerSection();
					}
				} else {
					m_CarryingItem.DroppedInLowerSection();
				}
				m_CarryingItem = null;
				if(ItemPutDown != null) {
					ItemPutDown();
				}
			}
		} else {
			if(Input.GetMouseButtonDown(0)) {
				RaycastHit movingUIHit;
				RaycastHit thirdPersonHit;
				var movingUIRay = GetMovingUIRay();
				if(Physics.Raycast(movingUIRay, out movingUIHit, 100f, m_ItemAndThrowMask)) {
					var item = movingUIHit.transform.parent.GetComponent<Item>();
					if(item != null) {
						m_CarryingItem = item;
						m_CarryingItem.PickedUpInLowerSection();
						m_Offset = m_CarryingItem.transform.position - (movingUIHit.point);
						if(ItemPickedUp != null) {
							ItemPickedUp(m_CarryingItem);
						}
					} else if(movingUIHit.transform.gameObject.layer == LayerMask.NameToLayer("Throw")) {
						if(ThrowIconClicked != null) {
							ThrowIconClicked();
						}
					}
				} else if(Physics.Raycast(this.GetThirdPersonRay(), out thirdPersonHit, 100f, this.m_SquireMask)) {
					if(SquireClicked != null) {
						SquireClicked();
					}
				}
			}

			if(Input.GetMouseButton(1)) {
				m_ThirdPersonCamera.GetComponent<ThirdPersonCam>().LookButtonPressed();
			}
		}
	}
}