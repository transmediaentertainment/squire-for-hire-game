﻿using System.Collections.Generic;

namespace TransTech.Patterns.Memento
{
	/// <summary>
	/// The caretaker for a Memento.
	/// </summary>
	public class MementoCaretaker<T> where T : MementoObject
	{
		/// <summary>
		/// The stack of mementos.
		/// </summary>
		private Stack<T> m_Mementos = new Stack<T>();
		
		/// <summary>
		/// Pushs a memento onto the stack.
		/// </summary>
		/// <param name='mem'>The memento to push</param>
		public void PushMemento(T mem)
		{
			m_Mementos.Push(mem);
		}
		
		/// <summary>
		/// Pops a memento from the stack.
		/// </summary>
		/// <returns>The memento on the top</returns>
		public T PopMemento()
		{
			if (m_Mementos.Count == 0)
				return null;
			return m_Mementos.Pop();
		}
		
		/// <summary>
		/// Clears the memento stack
		/// </summary>
		public void Clear()
		{
			m_Mementos.Clear();
		}
		
		/// <summary>
		/// Gets the number of mementos in the stack.
		/// </summary>
		/// <value>The number of mementos in the stack.</value>
		public int Count { get { return m_Mementos.Count; } }
	}
}