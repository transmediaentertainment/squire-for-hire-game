﻿namespace TransTech.Patterns.Memento
{
	/// <summary>
	/// The memento originator interface.
	/// </summary>
	public interface IMementoOriginator<T>
	{
		/// <summary>
		/// Creates the memento.
		/// </summary>
		/// <returns>The memento</returns>
		T CreateMemento();
		
		/// <summary>
		/// Sets the memento.
		/// </summary>
		/// <param name='memento'>The memento to set</param>
		void SetMemento(T memento);
	}
}