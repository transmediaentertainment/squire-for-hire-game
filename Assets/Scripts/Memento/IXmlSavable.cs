﻿using System;
using System.Xml;
namespace TransTech.System.IO
{
	public interface IXmlSavable
	{
		bool SaveAsXml(XmlWriter xw);
		bool LoadFromXml(XmlReader xr);
	}
}