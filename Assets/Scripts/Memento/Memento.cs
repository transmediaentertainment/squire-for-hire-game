﻿namespace TransTech.Patterns.Memento
{
	/// <summary>
	/// Memento object.
	/// </summary>
	public abstract class MementoObject { }
}