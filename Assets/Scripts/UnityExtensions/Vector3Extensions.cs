﻿using UnityEngine;

public static class Vector3Extensions {
	public static Vector3 Midpoint(this Vector3 v1, Vector3 v2) {
		Vector3 returnVec;
		returnVec.x = (v1.x + v2.x) * 0.5f;
		returnVec.y = (v1.y + v2.y) * 0.5f;
		returnVec.z = (v1.z + v2.z) * 0.5f;
		return returnVec;
	}
}