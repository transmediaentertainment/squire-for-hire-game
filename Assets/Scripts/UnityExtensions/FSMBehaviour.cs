﻿using UnityEngine;
using System.Collections;
using TransTech.System.Collections.Generic;
using TransTech.FiniteStateMachine;

// Wrapper around FSM and inherits Monobehaviour 
public class FSMBehaviour : MonoBehaviour {

	private FSM m_FSM;

	protected virtual void Awake() {
		m_FSM = new FSM ();
	}

	/// <summary>
	/// Pops all stacked states, and sets the given state as the new state
	/// </summary>
	/// <param name="newState">State to set as the new state</param>
	public void NewState(IFSMState newState, params object[] objs)
	{
		if (m_FSM != null) {
			m_FSM.NewState (newState, objs);
		} 
	}
	
	/// <summary>
	/// Pushes the new State onto the stack.  The Current State loses focus.
	/// </summary>
	/// <param name="newState">The new state to push onto the stack</param>
	public void PushState(IFSMState newState, params object[] objs)
	{
		m_FSM.PushState (newState, objs);
	}
	
	/// <summary>
	/// Pops the current state from the top of the stack and returns it.  If there
	/// is another state left in the stack, it regains focus.
	/// </summary>
	/// <returns>The state popped from the stack, or NULL if the stack is empty</returns>
	public IFSMState PopState()
	{
		return m_FSM.PopState ();
	}
	
	/// <summary>
	/// Peeks at the current state from the top of the stack.
	/// </summary>
	public IFSMState Peek()
	{
		return m_FSM.Peek ();
	}
}
