﻿using UnityEngine;
using System.Collections;

public static class StringExtensions {

	public static string RemoveClone(this string s) {
		var index = s.IndexOf ("(Clone)");
		if(index > 0) {
			return s.Remove(index, 7);
		}
		return s;
	}
}
