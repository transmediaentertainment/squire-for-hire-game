﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class InvPos {
	public Item Item { get; set; }
	public float Rotation { get; set; }
	public int XPos { get; set; }
	public int YPos { get; set; }


	public InvPos(Item item, float rotation, int xPos, int yPos) 
	{
		Item = item;
		Rotation = rotation;
		XPos = xPos;
		YPos = yPos;
	}
}

public class Inventory : MonoBehaviour {

	private List<InvPos> m_Items = new List<InvPos>();
	private Item m_TempStore = null;
	private const int TempStoreIndex = int.MaxValue;
	private int [,] m_Slots;
	public int m_Width = 6;
	public int m_Height = 5;
	public int m_TrashWidth = 2;
	public int m_TrashHeight = 2;
	public int m_TrashXOffset = -3;
	public int m_TrashYOffset = 0;
	public int m_TempWidth = 2;
	public int m_TempHeight = 2;
	public int m_TempXOffset = -4;
	public int m_TempYOffset = 0;
	public int m_ThrowWidth = 2;
	public int m_ThrowHeight = 2;
	public int m_ThrowXOffset = -8;
	public int m_ThrowYOffset = 3;

	public static readonly Vector3 PickUpSpawnPoint = new Vector3 (-3f, 3f, 0f);

	private Material m_Mat1;
	private Material m_Mat2;
	private Material m_HoverValidMat;
	private Material m_HoverInvalidMat;

	private float m_BackingOffset = 0.05f;

	public int m_ItemCount;

	public event Action InventoryChanged;

	private GameObject[,] m_BagSlots = null;
	private List<GameObject> m_TrashSlots = new List<GameObject>();
	private List<GameObject> m_TempSlots = new List<GameObject>();

	public static float SquareScale { get; private set; }
	public static Vector3 GridBottomLeft { get; private set; }

	private Vector3 m_TempStoreBottomLeft;

	public static Vector3 GetWorldGridPos(int x, int y) {
		return GridBottomLeft + new Vector3 (x * SquareScale, y * SquareScale, 0f);
	}

	public static int WorldToGridX(float x) {
		return Mathf.RoundToInt((x - GridBottomLeft.x) / SquareScale);
	}

	public static int WorldToGridY(float y) {
		return Mathf.RoundToInt ((y - GridBottomLeft.y) / SquareScale);
	}

#if UNITY_EDITOR
	private bool m_IsShuttingDown = false;
	private bool m_IsRegistered = false;
	void Awake() {
		UnityEditor.EditorApplication.playmodeStateChanged += PlayModeStateChanged;
		m_IsRegistered = true;
	}

	void PlayModeStateChanged() {
		m_IsShuttingDown = true;
	}
#endif

	public void Init(Camera movingUICamera, Transform circle, Material mat1, Material mat2, Material hoverValidMat, Material hoverInvalidMat, Material trashMat, Material tempMat, Material throwMat) {
		m_Mat1 = mat1;
		m_Mat2 = mat2;
		m_HoverValidMat = hoverValidMat;
		m_HoverInvalidMat = hoverInvalidMat;
		m_Slots = new int[m_Width, m_Height];
		m_BagSlots = new GameObject[m_Width, m_Height];

		var gridBottomLeftRay = movingUICamera.ViewportPointToRay (new Vector3 (0.33f, 0f, 0f));
		var gridTopRightRay = movingUICamera.ViewportPointToRay (new Vector3 (1f, 0.66f, 0f));
		var farBottomLeftRay = movingUICamera.ViewportPointToRay (Vector3.zero);
		var farTopRightRay = movingUICamera.ViewportPointToRay (new Vector3 (1f, 1f, 0f));

		RaycastHit gridBottomLeftHit;
		RaycastHit gridTopRightHit;
		RaycastHit farBottomLeftHit;
		RaycastHit farTopRightHit;

		var backingLayer = LayerMask.GetMask ("Backing");

		Physics.Raycast (gridBottomLeftRay, out gridBottomLeftHit, 100f, backingLayer);
		Physics.Raycast (gridTopRightRay, out gridTopRightHit, 100f, backingLayer);
		Physics.Raycast (farBottomLeftRay, out farBottomLeftHit, 100f, backingLayer);
		Physics.Raycast (farTopRightRay, out farTopRightHit, 100f, backingLayer);

		var insetAmount = (farTopRightHit.point.x - farBottomLeftHit.point.x) * 0.03f;
		var gridWidth = gridTopRightHit.point.x - gridBottomLeftHit.point.x - insetAmount * 2f;
		var gridHeight = gridTopRightHit.point.y - gridBottomLeftHit.point.y - insetAmount * 2f;

		var measuredAspect = gridWidth / gridHeight;
		var gridAspect = (float)m_Width / (float)m_Height;


		if (measuredAspect > gridAspect) {
			SquareScale = gridHeight / m_Height;
			float space = gridWidth - (SquareScale * m_Width);
			GridBottomLeft = gridBottomLeftHit.point + new Vector3 (SquareScale * 0.5f + insetAmount + space * 0.5f, SquareScale * 0.5f + insetAmount, 0f);
		} else {
			SquareScale = gridWidth / m_Width;
			GridBottomLeft = gridBottomLeftHit.point + new Vector3 (SquareScale * 0.5f + insetAmount, SquareScale * 0.5f + insetAmount, 0f);
		}

		var movingUILayer = LayerMask.NameToLayer ("MovingUI");

		// Create Bag Slots
		for(int x = 0; x < m_Width; x++) {
			for(int y = 0; y < m_Height; y++) {
				var slot = GameObject.CreatePrimitive(PrimitiveType.Quad);
				slot.layer = LayerMask.NameToLayer("Inventory");
				slot.transform.position = GridBottomLeft + new Vector3(x * SquareScale, y * SquareScale, m_BackingOffset);
				slot.transform.localScale = new Vector3(SquareScale, SquareScale, 1f);
				slot.transform.SetParent(transform);
				slot.name = "Bag Slot " + x + "," + y;
				slot.GetComponent<Renderer>().sharedMaterial = (x + y) % 2 == 0 ? mat1 : mat2;
				Destroy(slot.GetComponent<Collider>());
				m_BagSlots[x,y] = slot;
			}
		}

		// Create Trash Zone
		for (int x = 0; x < m_TrashWidth; x++) {
			for (int y = 0; y < m_TrashHeight; y++) {
				var slot = GameObject.CreatePrimitive(PrimitiveType.Quad);
				slot.transform.position = farBottomLeftHit.point + new Vector3((x + 0.5f) * Inventory.SquareScale + insetAmount, (y + 0.5f) * Inventory.SquareScale + insetAmount, 0f);
				slot.transform.localScale = new Vector3(Inventory.SquareScale, Inventory.SquareScale, 1f);
				slot.GetComponent<Renderer>().sharedMaterial = trashMat; 
				slot.transform.SetParent(transform);
				slot.name = "Trash Slot " + x + " " + y;
				var meshFilter = slot.GetComponent<MeshFilter>();
				meshFilter.mesh.uv = MeshUtilities.GetUVsForPositionedQuad(x, y, m_TrashWidth, m_TrashHeight, meshFilter.mesh);
				slot.layer = LayerMask.NameToLayer("Trash");
				m_TrashSlots.Add(slot);
			}
		}

		// Create Temp Store Zone
		m_TempXOffset = m_Width + 1;
		m_TempStoreBottomLeft = GridBottomLeft + new Vector3 (SquareScale * m_Width * 0.5f - SquareScale, SquareScale * m_Height + insetAmount, 0f);
		for (int x = 0; x < m_TempWidth; x++) {
			for(int y = 0; y < m_TempHeight; y++) {
				var slot = GameObject.CreatePrimitive(PrimitiveType.Quad);
				slot.transform.position = new Vector3(m_TempStoreBottomLeft.x + x * SquareScale, m_TempStoreBottomLeft.y + y * SquareScale, m_BackingOffset);
				slot.transform.localScale = new Vector3(Inventory.SquareScale, Inventory.SquareScale, 1f);
				slot.GetComponent<Renderer>().sharedMaterial = tempMat;
				slot.transform.SetParent(transform);
				slot.name = "Temp Slot " + x + " " + y;
				var meshFilter = slot.GetComponent<MeshFilter>();
				meshFilter.mesh.uv = MeshUtilities.GetUVsForPositionedQuad(x, y, m_TempWidth, m_TempHeight, meshFilter.mesh);
				slot.layer = LayerMask.NameToLayer("TempStore");
				m_TempSlots.Add(slot);
			}
		}

		circle.position = new Vector3 (m_ThrowXOffset + 0.5f, m_ThrowYOffset + 0.5f, 0f);
		circle.gameObject.layer = movingUILayer;
	}

	void OnDestroy() {
#if UNITY_EDITOR
		if(m_IsRegistered) {
			UnityEditor.EditorApplication.playmodeStateChanged -= PlayModeStateChanged;
		}
		if(m_IsShuttingDown) {
			return;
		}
#endif
		if (m_BagSlots != null) {
			for (int x = 0; x < m_BagSlots.GetLength(0); x++) {
				for(int y = 0; y < m_BagSlots.GetLength(1); y++) {
					Destroy(m_BagSlots[x,y]);
				}
			}
		}

		m_BagSlots = null;

		for(int i = 0; i < m_TrashSlots.Count; i++) {
			Destroy(m_TrashSlots[i]);
		}
		m_TrashSlots.Clear ();

		for(int i = 0; i < m_TempSlots.Count; i++) {
			Destroy(m_TempSlots[i]);
		}
		m_TempSlots.Clear ();

		for(int i = 0; i < m_Items.Count; i++) {
			m_Items[i].Item.CleanUp();
			Destroy(m_Items[i].Item.gameObject);
		}
		m_Items.Clear ();
	}


	public bool IsSlotInRangeAndEmpty(int x, int y) {
		//Debug.Log ("Is Range : " + x + " : " + y);
		// Is it in range
		if (x < 0 || x >= m_Width || y < 0 || y >= m_Height) {
			return false;
		}
		// Is the slot empty?
		return m_Slots [x, y] == 0;
	}

	public void ClearHovered() {
		for (int x = 0; x < m_BagSlots.GetLength(0); x++) {
			for (int y = 0; y < m_BagSlots.GetLength(1); y++) {
				m_BagSlots[x,y].GetComponent<Renderer>().sharedMaterial = (x + y) % 2 == 0 ? m_Mat1 : m_Mat2;
			}
		}
	}

	public void SetHovered(int x, int y) {
		if (!IsInBagRange (x, y)) {
			return;
		}
		if(m_Slots[x,y] > 0) {
			// Not valid
			m_BagSlots[x,y].GetComponent<Renderer>().sharedMaterial = m_HoverInvalidMat;
		} else {
			// Valid
			m_BagSlots[x,y].GetComponent<Renderer>().sharedMaterial = m_HoverValidMat;
		}
	}

	private bool IsInBagRange(int x, int y) {
		return x >= 0 && x < m_BagSlots.GetLength (0) && y >= 0 && y < m_BagSlots.GetLength (1);
	}

	public void AddItem(Item item, float rotation, int xPos, int yPos)
	{
		var invPos = new InvPos (item, rotation, xPos, yPos);
		AddItem (invPos);
	}

	public void AddItem(InvPos invPos) {
		
		m_Items.Add (invPos);
		var slotsFilled = invPos.Item.m_SlotsFilled;
		for (int x = 0; x < slotsFilled.GetLength(0); x++) {
			for(int y = 0; y < slotsFilled.GetLength(1); y++) {
				if(slotsFilled[x,y]) {
					var adjustedX = x;
					var adjustedY = y;
					if(invPos.Rotation.IsApproximately(90f)) {
						adjustedX = -y;
						adjustedY = x;
						
					} else if(invPos.Rotation.IsApproximately(180f)) {
						adjustedX = -x;
						adjustedY = -y;
						
					} else if(invPos.Rotation.IsApproximately(270f)) {
						adjustedX = y;
						adjustedY = -x;
					}
					m_Slots[adjustedX + invPos.XPos, adjustedY + invPos.YPos]++;
				}
			}
		}
		if (InventoryChanged != null) {
			InventoryChanged();
		}
	}

	public bool PutItemInTempStore(Item item) {
		if (m_TempStore != null) {
			return false;
		}

		m_TempStore = item;
		item.transform.position = m_TempStoreBottomLeft;
		if (InventoryChanged != null) {
			InventoryChanged();
		}
		return true;
	}

	public Item TakeItemFromTempStore() {
		var item = m_TempStore;
		m_TempStore = null;
		return item;
	}

	public InvPos RemoveItem(Item item) {
		for(int i = 0; i < m_Items.Count; i++) {
			if(m_Items[i].Item == item) {
				var removedItem = m_Items[i];
				var slotsFilled = removedItem.Item.m_SlotsFilled;
				for (int x = 0; x < slotsFilled.GetLength(0); x++) {
					for(int y = 0; y < slotsFilled.GetLength(1); y++) { 
						if(slotsFilled[x,y]) {
							var adjustedX = x;
							var adjustedY = y;
							if(removedItem.Rotation.IsApproximately(90f)) {
								adjustedX = -y;
								adjustedY = x;
								
							} else if(removedItem.Rotation.IsApproximately(180f)) {
								adjustedX = -x;
								adjustedY = -y;
								
							} else if(removedItem.Rotation.IsApproximately(270f)) {
								adjustedX = y;
								adjustedY = -x;
							}
							m_Slots[adjustedX + removedItem.XPos, adjustedY + removedItem.YPos]--;
						}
					}
				}
				m_Items.RemoveAt(i);
				if(InventoryChanged != null) {
					InventoryChanged();
				}
				return removedItem;
			}
		}
		return null;
	}

	public int GetInventoryValue() {
		int total = 0;
		for (int i = 0; i < m_Items.Count; i++) {
			if(m_Items[i].Item.m_IsJunk) {
				total += m_Items[i].Item.m_Value;
			}
		}
		if (m_TempStore != null && m_TempStore.m_IsJunk) {
			total += m_TempStore.m_Value;
		}
		return total;
	}

	public bool ContainsItemsWithNames(Item[] items) {
		int[] indices = new int[items.Length];
		for (int i = 0; i < indices.Length; i++) {
			indices[i] = -1;
		}
		for(int i = 0; i < items.Length; i++) {
			for(int x = 0; x < m_Items.Count; x++) {
				if(m_Items[x].Item.name == items[i].name) {
					if(!Array.Exists(indices, delegate(int index) { return index == x; })) {
						indices[i] = x;
						break;
					}
				}
			}
		}
		if (m_TempStore != null) {
			for (int i = 0; i < items.Length; i++) {
				if(m_TempStore.name == items[i].name && indices[i] == -1) {
					indices[i] = TempStoreIndex;
				}
			}
		}
		if (Array.Exists (indices, (index) => {
			return index == -1; })) {
			// Failed
			return false;
		}
		return true;
	}

	void Update() {
		m_ItemCount = m_Items.Count;
	}
}
