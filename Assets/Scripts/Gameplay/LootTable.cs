﻿using UnityEngine;
using System.Collections;

public class LootTable : MonoBehaviour {
	private struct WeightRange
	{
		public int Start;
		public int End;
		public GameObject Item;

		public WeightRange(GameObject item) {
			Item = item;
			Start = 0;
			End = 0;
		}

		public bool IsInRange(int query) {
			return (query >= Start && query <= End);
		}
	}
	public ItemTableEntry[] m_Items;
	private WeightRange[] m_ItemWeightRanges;

	int m_TotalWeight;

	public void InitTable() {
		if (m_ItemWeightRanges != null)
			return;
		m_ItemWeightRanges = new WeightRange[m_Items.Length];
		m_TotalWeight = 0;
		for (int i = 0; i < m_Items.Length; i++) {
			var range = new WeightRange(m_Items[i].m_Item);
			range.Start = m_TotalWeight;
			m_TotalWeight += m_Items[i].m_Weight;
			range.End = m_TotalWeight - 1;
			m_ItemWeightRanges[i] = range;
		}
	}

	public GameObject GetRandomItemPrefab() {
		var rand = Random.Range (0, m_TotalWeight);
		for(int i = 0; i < m_ItemWeightRanges.Length; i++) {
			if(m_ItemWeightRanges[i].IsInRange(rand)) {
				return m_ItemWeightRanges[i].Item;
			}
		}
		Debug.LogError("Rand wasn't in range! : " + rand + " : " + m_TotalWeight);
		return null;
	}
}
