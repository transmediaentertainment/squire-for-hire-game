﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemySpawnRules : MonoBehaviour {
	public GameObject m_EnemyPrefab;
	public int m_SpawnCount;
	public Item[] m_QuestItems;
	public LootTable m_LootTable;


	public EnemyController[] SpawnEnemies() {
		if (m_QuestItems != null && m_SpawnCount < m_QuestItems.Length) {
			Debug.LogError("Less enemies than quest item overrides requested : " + m_EnemyPrefab.name);
			return null;
		}

		m_LootTable.InitTable ();

		var enemies = new EnemyController[m_SpawnCount];
		for (int i = 0; i < m_SpawnCount; i++) {
			var go = GameObject.Instantiate(m_EnemyPrefab);
			enemies[i] = go.GetComponent<EnemyController>();
		}

		if (m_QuestItems != null) {
			// Quick check to see if we need to randomize
			if(m_QuestItems.Length == m_SpawnCount) {
				for(int i = 0; i < m_SpawnCount; i++) {
					enemies[i].OverrideLootTable(m_QuestItems[i]);
				}
			} else {
				int[] indices = new int[enemies.Length];
				for(int i = 0; i < indices.Length; i++) {
					indices[i] = i;
				}
				indices.Shuffle();
				for(int i = 0; i < m_QuestItems.Length; i++) {
					enemies[indices[i]].OverrideLootTable(m_QuestItems[i]);
				}
			}
		}

		for (int i = 0; i < enemies.Length; i++) {
			enemies[i].SetDropItem(m_LootTable.GetRandomItemPrefab ().GetComponent<Item>());
		}

		return enemies;
	}
}
