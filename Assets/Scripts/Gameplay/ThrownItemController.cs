﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class ThrownItemController : MonoBehaviour {

	private static List<ThrownItemController> m_ActiveItems = new List<ThrownItemController>();
	public static void CleanThrownItems() {
		for (int i = 0; i < m_ActiveItems.Count; i++) {
			m_ActiveItems[i].m_Item.CleanUp();
			Destroy(m_ActiveItems[i].m_Item.gameObject);
			Destroy(m_ActiveItems[i].gameObject);
		}
		m_ActiveItems.Clear ();
	}


	private KnightController m_Knight;
	private SquireController m_Squire;
	private float m_Speed;
	private float m_MinDist = 0.5f;
	private bool m_KnightReceiver;
	private Item m_Item;
	public event Action WentPastSquire;

	private void Awake() {
		m_ActiveItems.Add (this);
	}

	private void OnDestroy() {
		m_ActiveItems.Remove (this);
	}

	public void Init(Item item, float speed, KnightController knight, SquireController squire, bool knightReceiver) {
		m_Item = item;
		m_Speed = speed;
		m_Knight = knight;
		m_Squire = squire;
		m_KnightReceiver = knightReceiver;

		GetComponent<Renderer> ().material = item.m_Material;

		transform.position = m_KnightReceiver ? m_Squire.transform.position : m_Knight.transform.position;
	}

	public Item GetItem() {
		return m_Item;
	}

	private void Update() {
		var pos = transform.position;
		pos.z += Time.deltaTime * (m_KnightReceiver ? m_Speed : -m_Speed);
		transform.position = pos;

		bool received = false;
		var posTest = m_KnightReceiver ? m_Knight.transform.position : m_Squire.transform.position;
		if (m_KnightReceiver && pos.z >= posTest.z - m_MinDist) {
			// Done
			m_Item.ReceivedByKnight(m_Knight);
			received = true;

		} else if (!m_KnightReceiver && pos.z <= posTest.z + m_MinDist) {
			// Done
			if(WentPastSquire != null) {
				WentPastSquire();
			}
//			m_Item.ReceivedBySquire(m_Squire);
			received = true;
		}

		if (received) {
			if(!(m_Item is WeaponItem)) {
				m_Item.CleanUp();
				Destroy (m_Item.gameObject);
			}
			Destroy(gameObject);
		}
	}
}
