﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class SquireController : FSMBehaviour {

	private SquireStartingState m_StartState;
	private SquireIdleState m_IdleState;
	private SquireFollowState m_FollowState;
	private SquireBonusTargetState m_BonusTargetState;
	private SquireBagOpenState m_BagOpenState;
	private SquireFinishingState m_FinishingState;
	private SquireCatchingState m_CatchingState;

	private InputController m_InputController;
	public float m_MoveSpeed;
	private bool m_IsSprinting = false;
	public bool IsSprinting { get { return m_IsSprinting; } }
	private KnightController m_Knight;
	private Vector3 m_TargetOffset = new Vector3(0f, 0f, -0.5f);
	public Vector3 TargetOffset { get { return m_TargetOffset; } }
	private Vector3 m_OffScreenTarget = new Vector3(12f, 8.2f, 0f);
	public Vector3 OffScreenTarget { get { return m_OffScreenTarget; } }
	private PuzzleGameController m_PGC;
	private List<DroppedItem> m_DroppedItems = new List<DroppedItem> ();
	public int DroppedItemsCount { get { return m_DroppedItems.Count; } }
	public DroppedItem NextDroppedItem {
		get {
			return (m_DroppedItems.Count > 0) ? m_DroppedItems[0] : null;
		}
	}

	private bool m_HeadToFinish = false;
	public event Action ReachedEnd;
	// TODO : Handle closing bag on quest item case.
	public event Action ClosedBag;

	public bool IsSquirePickingUp { get { return Peek () == m_BagOpenState; } }

	public string m_CurrentState = "";

#if UNITY_EDITOR
	private bool m_IsShuttingDown = false;
	private bool m_IsRegistered = false;

	protected override void Awake ()
	{
		base.Awake ();
		UnityEditor.EditorApplication.playmodeStateChanged += PlayModeStateChanged;
		m_IsRegistered = true;
	}
	
	void PlayModeStateChanged() {
		m_IsShuttingDown = true;
	}
#endif

	void OnDestroy() {
#if UNITY_EDITOR
		if(m_IsRegistered) {
			UnityEditor.EditorApplication.playmodeStateChanged -= PlayModeStateChanged;
		}
		if(m_IsShuttingDown) {
			return;
		}
#endif
		if (m_InputController != null) {
			m_InputController.SquireClicked -= HandleSquireClicked;
		}
	}

	void HandleReachedEnd ()
	{
		NewState (m_IdleState);
		if (ReachedEnd != null) {
			ReachedEnd();
		}
	}

	void HandleItemReached ()
	{
		m_DroppedItems[0].PickUpItem();
		m_DroppedItems.RemoveAt(0);
		NewState (m_BagOpenState);
	}

	public void Init(InputController inputController, KnightController knight, PuzzleGameController pgc, Vector3 offscreenTarget) {
		m_InputController = inputController;
		m_InputController.SquireClicked += HandleSquireClicked;
		m_Knight = knight;

		m_Knight.WeaponThrown += HandleWeaponThrown;
		m_PGC = pgc;
		m_PGC.ItemDroppedByEnemy += HandleItemDroppedByEnemy;
		m_OffScreenTarget = offscreenTarget;

		transform.position = m_Knight.transform.position + m_TargetOffset;

		m_StartState = new SquireStartingState (knight);
		m_StartState.KnightNotWaitingForWeapon += HandleKnightNotWaitingForWeapon;
		m_IdleState = new SquireIdleState ();
		m_FollowState = new SquireFollowState (this, m_Knight);
		m_FollowState.ItemReached += HandleItemReached;
		m_BagOpenState = new SquireBagOpenState ();
		m_FinishingState = new SquireFinishingState (this);
		m_FinishingState.ReachedEnd += HandleReachedEnd;
		m_CatchingState = new SquireCatchingState ();
		
		NewState (m_StartState);
	}

	void HandleSquireClicked ()
	{
		var current = Peek ();
		if (current == m_BagOpenState) {
			if(m_PGC.IsPickupItemQuestItem) {
				AudioManager.Instance.SpawnAudio(AudioEvent.General_FailDropItem, transform.position, Quaternion.identity);
			} else {
				SetToFollowing();
				if(ClosedBag != null) {
					ClosedBag();
				}
			}
		} else if(current == m_FollowState || current == m_BonusTargetState)  {
			NewState(m_BagOpenState);
		}
	}

	void HandleKnightNotWaitingForWeapon ()
	{
		SetToFollowing ();
	}

	void HandleWeaponThrown (WeaponItem obj)
	{
		PushState (m_CatchingState);
	}

	void HandleItemDroppedByEnemy (DroppedItem obj)
	{
		m_DroppedItems.Add (obj);
	}

	public void HandleWeaponDropped(DroppedItem weapon) {
		if (m_DroppedItems.Count == 0) {
			m_DroppedItems.Add (weapon);
		} else {
			m_DroppedItems.Insert(1, weapon);
		}
		if (Peek () == m_CatchingState) {
			PopState ();
		} else {
			Debug.LogError("Weapon was dropped but Squire wasn't in Catching State.");
		}
	}

	public void SetToFinishing() {
		m_HeadToFinish = true;
	}

	private void SetToFollowing() {
		bool wasCatching = Peek () == m_CatchingState;

		if (m_HeadToFinish && m_DroppedItems.Count == 0) {
			NewState (m_FinishingState);
		} else {
			NewState (m_FollowState);
		}

		if (wasCatching) {
			PushState(m_CatchingState);
		}
	}

	public void SprintToEnd() {
		m_IsSprinting = true;
		NewState (m_FinishingState);
	}

	void Update() {
		var current = Peek ();
		if (current != null) {
			m_CurrentState = current.GetType().ToString();
		}
	}
}
