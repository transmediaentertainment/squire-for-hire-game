﻿using UnityEngine;
using System;
using System.Collections;

public class KnightController : MonoBehaviour {
	public enum KnightState {
		WaitingForWeapon,
		Idle,
		Moving, 
		Fighting
	}

	public float m_MoveSpeed;
	public int m_MaxHealth;
	public int m_Health;
	public int m_Strength;
	public float m_AttacksPerSecond; 

	private KnightState m_State;
	public KnightState State { get { return m_State; } }

	public Vector3 m_NextTarget;
	private Vector3 m_TargetOffset = new Vector3(0f, 0f, -0.3f);
	private Vector3 m_OffScreenTarget = new Vector3(12f, 8.2f, 0f);
	private float m_AttackTimer;
	private float m_SwingTimer;
	private float m_SwingTime;
	private int m_SwingAudioSourceIndex;
	private float m_AttackRate;
	private EnemyController m_NextEnemy;

	public WeaponItem m_CurrentWeapon;
	public event Action<WeaponItem> WeaponThrown;

	public event Action ReachedEnd;
	public event Action<int, int> HealthChanged;
	public event Action KnightAttackLanded;
	public event Action KnightDied;

	private GameObject m_SpawnedAudioObject = null;

	private readonly Vector3 m_ParticleRotation = new Vector3 (-30f, 270f, -90f);

	public GameObject m_SpeechBubble;
	public GameObject m_WeaponInBubble;
	public Material m_SwordMat;
	public Material m_AxeMat;
	public Material m_MaceMat;
	public Material m_ClubMat;
	public Material m_SpearMat;

	void Awake() {
		m_AttackRate = 1f / m_AttacksPerSecond;
		m_Health = m_MaxHealth;
		if (HealthChanged != null) {
			HealthChanged(m_Health, m_MaxHealth);
		}
	}

	public void Init(Vector3 endTarget) {
		m_OffScreenTarget = endTarget + new Vector3(0f, 0f, 5f);
	}

	public void CleanUp() {
		if (m_CurrentWeapon != null) {
			m_CurrentWeapon.CleanUp();
			Destroy(m_CurrentWeapon.gameObject);
		}
	}

	public void SetNextEnemy(EnemyController enemy) {
		m_NextEnemy = enemy;
		if (m_CurrentWeapon != null) {
			m_State = KnightState.Moving;
		} else {
			m_State = KnightState.WaitingForWeapon;
		}
		RequestBestWeapon (enemy);
		m_NextTarget = m_NextEnemy.transform.position;
		m_SwingAudioSourceIndex = AudioManager.Instance.GetAudioSourceIndex (AudioEvent.Knight_Swing);
		var swingLength = AudioManager.Instance.GetAudioSourceLength (AudioEvent.Knight_Swing, m_SwingAudioSourceIndex);
		if (swingLength == 0) {
			// no swing audio - invalidate the time
			m_SwingTime = -1.0f;
		} else {
			m_SwingTime = m_AttackRate - swingLength;
			if (m_SwingTime < 0) {
				m_SwingTime = 0;
			}
			//Debug.Log("Swing Time Set:" + m_SwingTime + " AR:" + m_AttackRate + " SL:" + swingLength);
		}
	}

	private void RequestBestWeapon(EnemyController enemy) {
		if (m_CurrentWeapon != null && m_CurrentWeapon.WeaponType == enemy.m_WeaponWeakness) {
			// Already have best weapon
			m_SpeechBubble.SetActive(false);
			return;
		}

		m_SpeechBubble.SetActive (true);
		var weaponRenderer = m_WeaponInBubble.GetComponent<MeshRenderer> ();
		Material weaponMat = null;
		switch (enemy.m_WeaponWeakness) {
		case WeaponType.Axe:
			weaponMat = m_AxeMat;
			break;
		case WeaponType.Club:
			weaponMat = m_ClubMat;
			break;
		case WeaponType.Mace:
			weaponMat = m_MaceMat;
			break;
		case WeaponType.Spear:
			weaponMat = m_SpearMat;
			break;
		case WeaponType.Sword:
			weaponMat = m_SwordMat;
			break;
		default:
			Debug.LogError("Knight not set up to request for wepaon type : " + enemy.m_WeaponWeakness);
			break;
		}
		weaponRenderer.sharedMaterial = weaponMat;
		if (weaponMat != null) {
			var ratio = (float)weaponMat.mainTexture.width / (float)weaponMat.mainTexture.height;
			m_WeaponInBubble.transform.localScale = new Vector3(ratio * 0.5f, 0.5f, 1f);
		}
	}

	public void SetOffScreenTarget() {
		m_NextTarget = m_OffScreenTarget;// + new Vector3(screenXCenter, 0f, 0f);
		m_State = KnightState.Moving;
		m_NextEnemy = null;
	}

	void Update() {
		switch (m_State) {
		case KnightState.Idle:
		case KnightState.WaitingForWeapon:
			// Do nothing
			break;
		case KnightState.Moving:
			var pos = transform.position;
			pos.z += m_MoveSpeed * Time.deltaTime;
			if(pos.z >= (m_NextTarget + m_TargetOffset).z) {
				pos.z = (m_NextTarget + m_TargetOffset).z;
				if(m_NextEnemy != null) {
					m_State = KnightState.Fighting;
					m_AttackTimer = 0f;
					m_SwingTimer = 0f;
				} else {
					// We've reached the end
					m_State = KnightState.Idle;
					if(ReachedEnd != null) {
						ReachedEnd();
					}
				}
			}
			transform.position = pos;
			break;
		case KnightState.Fighting:
			m_AttackTimer += Time.deltaTime;
			m_SwingTimer += Time.deltaTime;
			if ( m_SwingTimer >= 0 && m_SwingTimer >= m_SwingTime ) {
				m_SwingTimer = -1.0f;
				//Debug.Log("Swing");
				if ( m_SpawnedAudioObject != null && m_SpawnedAudioObject.GetComponent<AudioSource> ().isPlaying) {
					m_SpawnedAudioObject.GetComponent<AudioSource> ().Stop ();
				}
				m_SpawnedAudioObject = AudioManager.Instance.SpawnAudio (AudioEvent.Knight_Swing, m_SwingAudioSourceIndex, transform.position, transform.rotation);
			}
			if(m_AttackTimer >= m_AttackRate) {
				if ( m_SpawnedAudioObject != null && m_SpawnedAudioObject.GetComponent<AudioSource> ().isPlaying) {
					m_SpawnedAudioObject.GetComponent<AudioSource> ().Stop ();
				}
				m_SpawnedAudioObject = AudioManager.Instance.SpawnAudio (AudioEvent.Weapon_SwordHit_Flesh, transform.position, transform.rotation);
				m_NextEnemy.ApplyDamage(this, m_Strength);
				m_AttackTimer -= m_AttackRate;
				if(KnightAttackLanded != null) {
					KnightAttackLanded();
				}
			}
			break;
		}
	}

	public void CatchWeapon(WeaponItem weapon) {
		AudioManager.Instance.SpawnAudio (AudioEvent.Knight_Catch, transform.position, transform.rotation);
		if (m_CurrentWeapon != null) {
			ThrowCurrentWeapon();
		}
		m_CurrentWeapon = weapon;
		RequestBestWeapon (m_NextEnemy);
		if (m_NextEnemy != null && m_State == KnightState.WaitingForWeapon) {
			m_State = KnightState.Moving;
		}
	}

	private void ThrowCurrentWeapon() {
		if (m_CurrentWeapon != null) {
			AudioManager.Instance.SpawnAudio (AudioEvent.General_ThrowItem, transform.position, transform.rotation);
			if(WeaponThrown != null) {
				WeaponThrown(m_CurrentWeapon);
			}
		}
	}

	public void ApplyHealing(int healAmount) {
		m_Health = Mathf.Min(m_MaxHealth, m_Health + healAmount);
		AudioManager.Instance.SpawnAudio (AudioEvent.Knight_HealthUp, transform.position, transform.rotation);
		if (HealthChanged != null) {
			HealthChanged(m_Health, m_MaxHealth);
		}
	}

	public void ApplyDamage(int damage) {
		m_Health -= damage;
		ParticlesManager.Instance.SpawnParticles (ParticleTemplate.KnightGotHit, transform.position + new Vector3 (0f, 0f, -0.1f), Quaternion.Euler (m_ParticleRotation));
		if (HealthChanged != null) {
			HealthChanged(m_Health, m_MaxHealth);
		}
		if (m_Health <= 0) {
			if ( m_SpawnedAudioObject != null && m_SpawnedAudioObject.GetComponent<AudioSource> ().isPlaying) {
				m_SpawnedAudioObject.GetComponent<AudioSource> ().Stop ();
			}
			m_SpawnedAudioObject = AudioManager.Instance.SpawnAudio (AudioEvent.Knight_Death, transform.position, transform.rotation);
			if (KnightDied != null) {
				KnightDied ();
			}
		} else {
			if (m_SpawnedAudioObject == null || !m_SpawnedAudioObject.GetComponent<AudioSource> ().isPlaying) {
				m_SpawnedAudioObject = AudioManager.Instance.SpawnAudio (AudioEvent.Knight_GotHit, transform.position, transform.rotation);
			}
		}
	}
}
