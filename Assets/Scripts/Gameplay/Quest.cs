﻿using UnityEngine;
using System.Collections.Generic;

public partial class Quest : MonoBehaviour {

	public string m_Owner; // Who you're working for
	public string m_Description;
	public EnemySpawnRules[] m_EnemySpawnRules;
	public int m_Seed;
	public KnightController m_KnightController;
	public float m_StartDist = 3f;
	public float m_EndDist = 3f;
	public float m_EnemyDistanceMin = 0.75f;
	public float m_EnemyDistanceMax = 1.25f;

	public Item[] GetRequiredQuestItems() {
		int count = 0;
		for(int i =0; i < m_EnemySpawnRules.Length; i++) {
			if(m_EnemySpawnRules[i].m_QuestItems != null) {
				count += m_EnemySpawnRules[i].m_QuestItems.Length;
			}
		}

		if (count == 0) {
			return null;
		}

		var required = new Item[count];
		for (int i =0; i < m_EnemySpawnRules.Length; i++) {
			if (m_EnemySpawnRules [i].m_QuestItems != null) {
				for(int x = 0;x < m_EnemySpawnRules [i].m_QuestItems.Length; x++) {
					required[x] = m_EnemySpawnRules [i].m_QuestItems[x];
				}
			}
		}

		return required;
	}

	public bool IsQuestItem(Item item) {
		for (int i = 0; i < m_EnemySpawnRules.Length; i++) {
			if(m_EnemySpawnRules[i].m_QuestItems != null) {
				for(int x = 0; x < m_EnemySpawnRules[i].m_QuestItems.Length; x++) {
					if(item.name == m_EnemySpawnRules[i].m_QuestItems[x].name) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public EnemyController[]  Init() {
		Random.seed = m_Seed;

		int enemyCount = 0;

		for (int i = 0; i < m_EnemySpawnRules.Length; i++) {
			enemyCount += m_EnemySpawnRules[i].m_SpawnCount;
		}

		var enemies = new EnemyController[enemyCount];
		var index = 0;
		for (int i = 0; i < m_EnemySpawnRules.Length; i++) {
			var spawnedEnemies = m_EnemySpawnRules[i].SpawnEnemies();
			for(int x = 0; x < spawnedEnemies.Length; x++) {
				spawnedEnemies[x].name = "Enemy " + x + index;
				enemies[x + index] = spawnedEnemies[x];
			}
			index += spawnedEnemies.Length;
		}

		enemies.Shuffle ();

		return enemies;
	}

	public int KnightHealthAfterQuest() {

		if (m_KnightController == null) {
			return 0;
		}

		int knightHealth = m_KnightController.m_MaxHealth;

		for (int i = 0; i < m_EnemySpawnRules.Length; i++) {
			if(m_EnemySpawnRules[i] != null) {
				for( int j=0; j < m_EnemySpawnRules[i].m_SpawnCount; j++ ) {
					var enemyController = m_EnemySpawnRules[i].m_EnemyPrefab.GetComponent<EnemyController>();
					float enemyHealth = (float)enemyController.m_Health;
					int numberOfStrikesToKillEnemy = Mathf.CeilToInt( enemyHealth / (float)m_KnightController.m_Strength );
					float secondsToKillEnemy = (float)numberOfStrikesToKillEnemy / m_KnightController.m_AttacksPerSecond;
					int numberOfEnemyStrikes = Mathf.FloorToInt(secondsToKillEnemy / (1.0f / enemyController.m_AttacksPerSecond));
					int damageToKnight = numberOfEnemyStrikes * enemyController.m_Strength;
					knightHealth-= damageToKnight;
				}
			}
		}
		
		return knightHealth;

	}
}
