using UnityEngine;
using System;
using System.Collections;

public class DroppedItem : MonoBehaviour {

	private GameObject m_ItemGameObject;

	public event Action<GameObject> ItemPickedUp;

	public void Init(GameObject itemGameObject) {
		m_ItemGameObject = itemGameObject;
		m_ItemGameObject.SetActive (false);
		var item = m_ItemGameObject.GetComponent<Item> ();
		item.DroppedInUpperActionArea ();
		var texture = item.m_Material.mainTexture;
		gameObject.GetComponent<Renderer> ().material.mainTexture = texture;
		var width = item.m_SlotsFilled.GetLength (0);
		var height = item.m_SlotsFilled.GetLength (1);
		transform.localScale = new Vector3 (width * 0.2f, height * 0.2f, 1f);
	}

	public void PickUpItem() {
		m_ItemGameObject.SetActive (true);
		m_ItemGameObject.transform.position = Inventory.PickUpSpawnPoint;
		m_ItemGameObject.GetComponent<Item> ().PickedUpFromDroppedItems ();
		gameObject.SetActive (false);
		if (ItemPickedUp != null) {
			ItemPickedUp(m_ItemGameObject);
		}
	}
}
