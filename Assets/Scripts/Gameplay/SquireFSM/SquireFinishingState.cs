﻿using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;
using System;

public class SquireFinishingState : FSMState {

	public event Action ReachedEnd;
	private SquireController m_Squire;

	public SquireFinishingState(SquireController squire) {
		m_Squire = squire;
	}

	public override void Update (float deltaTime)
	{
		base.Update (deltaTime);

		var pos = m_Squire.transform.position;
		pos.z += m_Squire.m_MoveSpeed * Time.deltaTime * (m_Squire.IsSprinting ? 3f : 1f);
		if(pos.z >= m_Squire.OffScreenTarget.z) {
			if(ReachedEnd != null) {
				ReachedEnd();
			}
		}
		m_Squire.transform.position = pos;
	}
}
