﻿using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;
using System;

public class SquireFollowState : FSMState {

	private SquireController m_Squire;
	private KnightController m_Knight;

	public event Action ItemReached; 

	public SquireFollowState(SquireController squire, KnightController knight) {
		m_Squire = squire;
		m_Knight = knight;
	}

	public override void Update (float deltaTime)
	{
		base.Update (deltaTime);

		var pos = m_Squire.transform.position;
		pos.z += m_Squire.m_MoveSpeed * deltaTime;
		if(pos.z >= (m_Knight.transform.position + m_Squire.TargetOffset).z) {
			pos.z = (m_Knight.transform.position + m_Squire.TargetOffset).z;
		}
		var item = m_Squire.NextDroppedItem;
		if(item != null) {

			var itemPos = item.transform.position;
			if(pos.z >= itemPos.z) {
				pos.z = itemPos.z;
				if(ItemReached != null) {
					ItemReached();
				}
			}
		}
		m_Squire.transform.position = pos;
	}
}
