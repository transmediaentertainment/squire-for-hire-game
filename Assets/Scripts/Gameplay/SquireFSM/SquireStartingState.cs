﻿using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;
using System;

public class SquireStartingState : FSMState {

	KnightController m_Knight;

	public event Action KnightNotWaitingForWeapon;

	public SquireStartingState(KnightController knight) {
		m_Knight = knight;
	}

	public override void Update (float deltaTime)
	{
		base.Update (deltaTime);

		if (m_Knight.State != KnightController.KnightState.WaitingForWeapon) {
			if(KnightNotWaitingForWeapon != null) {
				KnightNotWaitingForWeapon();
			}
		}
	}
}
