﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class EnemyController : MonoBehaviour {

	public enum EnemyState
	{
		Idle,
		Combat
	}

	private Item m_DropItem;
	private Item m_OverrideItem;
	public WeaponType m_WeaponWeakness;
	public int m_MaxHealth;
	public int m_Health;
	public int m_Strength;
	public float m_AttacksPerSecond;
	private float m_AttackRate;
	private KnightController m_Knight;

	public event Action<Item> EnemyDied;

	private EnemyState m_State = EnemyState.Idle;
	private float m_AttackTimer = 0f;

	private readonly Vector3 m_ParticleRotation = new Vector3 (-30f, 90f, -90f);

	public AudioEvent m_EnemyGotHitSound;
	public AudioEvent m_EnemyDeathSound;
	public AudioEvent m_EnemyAttackSound;

	private GameObject m_SpawnedAudioObject = null;

	void Awake() {
		m_AttackRate = 1f / m_AttacksPerSecond;
		m_Health = m_MaxHealth;
	}

	public void OverrideLootTable(Item item) {
		m_OverrideItem = item;
	}

	public void SetDropItem(Item item) {
		m_DropItem = item;
	}

	public void ApplyDamage(KnightController knight, int damage) {
		if (knight.m_CurrentWeapon != null && knight.m_CurrentWeapon.WeaponType == m_WeaponWeakness) {
			damage *= 2;
		}
		m_Health -= damage;
		ParticlesManager.Instance.SpawnParticles (ParticleTemplate.EnemyGotHit, transform.position + new Vector3(0f, 0f, -0.1f), Quaternion.Euler(m_ParticleRotation));
		if (m_Health <= 0) {
			if ( m_SpawnedAudioObject != null && m_SpawnedAudioObject.GetComponent<AudioSource>().isPlaying ) {
				m_SpawnedAudioObject.GetComponent<AudioSource>().Stop ();
			}
			m_SpawnedAudioObject = AudioManager.Instance.SpawnAudio (m_EnemyDeathSound, transform.position, transform.rotation);
			if (EnemyDied != null) {
				gameObject.SetActive (false);
				if (m_OverrideItem != null) {
					EnemyDied (m_OverrideItem);
				} else {
					EnemyDied (m_DropItem);
				}
			}
		} else {
			if ( m_SpawnedAudioObject == null || !m_SpawnedAudioObject.GetComponent<AudioSource>().isPlaying ) {
				m_SpawnedAudioObject = AudioManager.Instance.SpawnAudio (m_EnemyGotHitSound, transform.position, transform.rotation);
			}
			m_Knight = knight;
			m_State = EnemyState.Combat;
		}
	}

	void Update() {
		switch(m_State) {
		case EnemyState.Idle:
			break;
		case EnemyState.Combat:
			m_AttackTimer += Time.deltaTime;
			if(m_AttackTimer >= m_AttackRate) {
				// Attack!
				if ( m_SpawnedAudioObject != null && m_SpawnedAudioObject.GetComponent<AudioSource>().isPlaying ) {
					m_SpawnedAudioObject.GetComponent<AudioSource>().Stop ();
				}
				m_SpawnedAudioObject = AudioManager.Instance.SpawnAudio (m_EnemyAttackSound, transform.position, transform.rotation);
				m_AttackTimer -= m_AttackRate;
				m_Knight.ApplyDamage(m_Strength);
			}
			break;
		}

	}
}
