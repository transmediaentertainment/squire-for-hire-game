﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class UIFSM : FSMBehaviour {

	// States
	private MainMenuUIState m_MainMenuState;
	private TownUIState m_TownState;
	private QuestReportUIState m_QuestReportState;
	private PuzzleGameUIState m_PuzzleGameState;

	// Main Menu UI
	public RectTransform m_MainMenuPanel;
	public Button m_NewGameButton;
	public Button m_LoadGameButton;
	
	// Town UI
	public RectTransform m_TownPanel;
	
	// Puzzle Game UI
	public RectTransform m_PuzzleGamePanel;
	public Text m_CurrentItemText;
	public Text m_CurrentItemValueText;
	public Text m_TotalValueText;
	public Text m_FinalCountdownText;
	public EnergyBar m_KnightHealthBar;
	
	// Quest Report UI
	public RectTransform m_QuestReportPanel;
	public Text m_QuestReportTitle;
	public Text m_QuestReportMessage;
	public Button m_QuestReportContinueButton;
	
	public InputController m_InputController;
	
	public GameObject m_ButtonPrefab;
	
	public PuzzleGameController m_PuzzleGameController;

	public MainGameFSM m_MainGameFSM;

	protected override void Awake ()
	{
		base.Awake ();
	
		m_MainMenuState = new MainMenuUIState (this, m_MainMenuPanel, m_MainGameFSM, m_NewGameButton, m_LoadGameButton);
		m_TownState = new TownUIState (this, m_TownPanel, m_MainGameFSM, m_ButtonPrefab);
		m_PuzzleGameState = new PuzzleGameUIState (this, m_PuzzleGamePanel, m_PuzzleGameController, m_InputController, m_CurrentItemText, m_CurrentItemValueText, m_TotalValueText, m_FinalCountdownText, m_KnightHealthBar);
		m_QuestReportState = new QuestReportUIState (this, m_QuestReportPanel, m_MainGameFSM, m_QuestReportTitle, m_QuestReportMessage, m_QuestReportContinueButton);

		NewState (m_MainMenuState);
	}

	public void ShowMainMenu() {
		NewState (m_MainMenuState);
	}

	public void ShowPuzzleGameUI ()
	{
		NewState (m_PuzzleGameState);
	}

	public void ShowQuestReportUI (bool success, string message) {
		NewState (m_QuestReportState, success, message);
	}

	public void ShowTownUI (Quest[] quests) {
		NewState (m_TownState, quests);
	}
}
