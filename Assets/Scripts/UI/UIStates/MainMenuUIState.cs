﻿using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;
using UnityEngine.UI;

public class MainMenuUIState : UIPanelState {
	
	private Button m_NewGameButton;
	private Button m_LoadGameButton;
	private MainGameFSM m_MainGameFSM;

	public MainMenuUIState(UIFSM fsm, RectTransform panel, MainGameFSM mainGameFSM, Button newGameButton, Button loadGameButton) : base(fsm, panel) {
		m_MainGameFSM = mainGameFSM;
		m_NewGameButton = newGameButton;
		m_LoadGameButton = loadGameButton;
	}

	public override void Enter (params object[] args)
	{
		base.Enter (args);

		m_NewGameButton.onClick.AddListener (HandleNewGameButtonClick);
		m_LoadGameButton.onClick.AddListener (HandleLoadGameButtonClick);
	}

	public override void Exit ()
	{
		base.Exit ();

		m_NewGameButton.onClick.RemoveListener (HandleNewGameButtonClick);
		m_LoadGameButton.onClick.RemoveListener (HandleLoadGameButtonClick);
	}

	private void HandleNewGameButtonClick() {
		m_MainGameFSM.NewGame ();
	}

	private void HandleLoadGameButtonClick() {
	}
}
