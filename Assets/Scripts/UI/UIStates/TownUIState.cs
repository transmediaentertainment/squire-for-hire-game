﻿using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;
using UnityEngine.UI;
using System.Collections.Generic;

public class TownUIState : UIPanelState {

	private GameObject m_ButtonPrefab;
	private Vector3 m_QuestButtonStart = Vector3.zero;
	private Vector3 m_QuestButtonOffset = new Vector3 (0f, -35f, 0f);

	private MainGameFSM m_MainGameFSM;

	private List<GameObject> m_QuestButtons = new List<GameObject>(); 

	public TownUIState(UIFSM fsm, RectTransform panel, MainGameFSM mainGameFSM, GameObject buttonPrefab) : base(fsm, panel) {
		m_ButtonPrefab = buttonPrefab;
		m_MainGameFSM = mainGameFSM;
	}

	public override void Enter (params object[] args)
	{
		base.Enter (args);

		if (args == null || args.Length == 0) {
			Debug.LogError("No Args passed into TownUIState Enter.");
			return;
		}

		var quests = (Quest[])args;

		for(int i = 0; i < quests.Length; i++) {
			var buttonGO = GameObject.Instantiate(m_ButtonPrefab);
			buttonGO.transform.SetParent(m_Panel);
			buttonGO.transform.localPosition = m_QuestButtonStart + i * m_QuestButtonOffset;
			var button = buttonGO.GetComponent<Button>();
			var q = quests[i];
			button.onClick.AddListener(() => {
				m_MainGameFSM.StartQuest(q);
			});
			var text = buttonGO.GetComponentInChildren<Text>();
			text.text = quests[i].m_Owner + " Quest";
			m_QuestButtons.Add(buttonGO);
		}
	}

	public override void Exit ()
	{
		base.Exit ();

		for (int i = 0; i < m_QuestButtons.Count; i++) {
			GameObject.Destroy(m_QuestButtons[i]);
		}
		m_QuestButtons.Clear ();
	}
}
