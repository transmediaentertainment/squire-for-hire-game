﻿using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;

public class UIPanelState : FSMState {

	protected UIFSM m_FSM;
	protected RectTransform m_Panel;

	public UIPanelState(UIFSM fsm, RectTransform panel) {
		m_FSM = fsm;
		m_Panel = panel;
		m_Panel.gameObject.SetActive (false);
	}

	public override void Enter (params object[] args)
	{
		base.Enter (args);

		m_Panel.gameObject.SetActive (true);
	}

	public override void Exit ()
	{
		base.Exit ();

		m_Panel.gameObject.SetActive (false);
	}
}
