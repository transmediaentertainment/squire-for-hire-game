﻿using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;
using UnityEngine.UI;

public class PuzzleGameUIState : UIPanelState {

	private PuzzleGameController m_PGC;
	private InputController m_InputController;

	private Text m_CurrentItemText;
	private Text m_CurrentItemValueText;
	private Text m_TotalValueText;
	private Text m_FinalCountdownText;
	private EnergyBar m_KnightEnergyBar;

	public PuzzleGameUIState(UIFSM fsm, RectTransform panel, PuzzleGameController pgc, InputController inputController,
	                        Text currentItemText, Text currentItemValueText, Text totalValueText, Text finalCountdownText,
	                        EnergyBar knightEnergyBar) : base(fsm, panel) {
		m_PGC = pgc;
		m_InputController = inputController;

		m_CurrentItemText = currentItemText;
		m_CurrentItemValueText = currentItemValueText;
		m_TotalValueText = totalValueText;
		m_FinalCountdownText = finalCountdownText;
		m_KnightEnergyBar = knightEnergyBar;
	}

	public override void Enter (params object[] args)
	{
		base.Enter (args);

		m_PGC.ValueUpdated += HandleValueUpdated;
		m_PGC.TimeRemainingUpdated += HandleTimeRemainingUpdated;
		m_PGC.KnightsHealthChanged += HandleKnightsHealthChanged;
		m_InputController.ItemPickedUp += HandleItemPickedUp;
		m_InputController.ItemPutDown += HandleItemPutDown;

		m_FinalCountdownText.gameObject.SetActive (false);

		SetItemText ("");
		SetItemValue (0);
		UpdateTotalValueText (0);
	}

	void HandleKnightsHealthChanged (int health, int maxHealth)
	{
		m_KnightEnergyBar.SetValueMax (maxHealth);
		m_KnightEnergyBar.SetValueCurrent (health);
	}

	void HandleTimeRemainingUpdated (float time)
	{
		m_FinalCountdownText.gameObject.SetActive (true);
		m_FinalCountdownText.text = "QUICKLY: " + time.ToString ("f2");
	}

	private void SetItemText(string itemName) {
		m_CurrentItemText.text = "Item: " + itemName;
	}

	private void SetItemValue(int itemValue) {
		m_CurrentItemValueText.text = "Value: $" + itemValue; 
	}
	
	private void UpdateTotalValueText(int value) {
		m_TotalValueText.text = "Total: $" + value;
	}

	void HandleItemPutDown ()
	{
		SetItemText ("");
		SetItemValue (0);
	}

	void HandleItemPickedUp (Item obj)
	{
		SetItemText (obj.name);
		SetItemValue (obj.m_Value);
	}

	void HandleValueUpdated (int value)
	{
		m_TotalValueText.text = "Total: $" + value;
	}
}