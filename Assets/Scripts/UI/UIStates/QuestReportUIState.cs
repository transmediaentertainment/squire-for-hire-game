﻿using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;
using UnityEngine.UI;

public class QuestReportUIState : UIPanelState {

	private MainGameFSM m_MainGameFSM;

	private Text m_QuestReportTitle;
	private Text m_QuestReportMessage;
	private Button m_QuestReportContinueButton;

	public QuestReportUIState(UIFSM fsm, RectTransform panel, MainGameFSM mainGameFSM, Text questReportTitle,
	                         Text questReportMessage, Button questReportContinueButton) : base(fsm, panel) {
		m_MainGameFSM = mainGameFSM;
		m_QuestReportTitle = questReportTitle;
		m_QuestReportMessage = questReportMessage;
		m_QuestReportContinueButton = questReportContinueButton;
	}

	public override void Enter (params object[] args)
	{
		base.Enter (args);

		if(args == null || args.Length != 2) {
			Debug.LogError("Incorrect Agrs passed into QuestReportUIState");
			return;
		}

		var success = (bool)args [0];
		var message = (string)args [1];

		m_QuestReportTitle.text = success ? "Quest Complete!" : "Quest Failed";
		m_QuestReportMessage.text = message;
		m_QuestReportContinueButton.onClick.AddListener (HandleQuestReportContinueClicked);
	}

	public override void Exit ()
	{
		base.Exit ();

		m_QuestReportContinueButton.onClick.RemoveListener (HandleQuestReportContinueClicked);
	}

	private void HandleQuestReportContinueClicked() {
		m_MainGameFSM.ReturnToTown ();
	}
}
