﻿using UnityEngine;
using System.Collections;

public class AudioCollection : MonoBehaviour {

	public AudioEvent m_AudioEvent;
	public bool m_Disable;
	public AudioSource[] m_AudioSourcePrefabs;

	public int GetAudioSourceIndex() {
		return Random.Range (0, m_AudioSourcePrefabs.Length);
	}

	public float GetAudioSourceLength( int audioSourceIndex ) {
		var prefab = GetAudioSourcePrefab (audioSourceIndex);
		if (prefab == null) {
			Debug.Log ("GetAudioSourceLength() for " + m_AudioEvent.ToString() + " Index:" + audioSourceIndex + " null prefab" );
			return 0f;
		}
		return prefab.clip.length;
	}

	public AudioSource GetAudioSourcePrefab(int audioSourceIndex) {
		if (m_AudioSourcePrefabs.Length == 0) {
			return null;
		}
		if (audioSourceIndex < 0 || audioSourceIndex >= m_AudioSourcePrefabs.Length) {
			Debug.Log ("GetAudioSourcePrefab() Error: m_AudioSourcePrefabs Index out of range:" + audioSourceIndex);
			audioSourceIndex = 0;
		}
		return m_AudioSourcePrefabs [audioSourceIndex];
	}

}
