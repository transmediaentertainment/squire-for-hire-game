﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PathologicalGames;

public enum AudioEvent 
{
	Enemy_RatGotHit,
	Enemy_RatDeath,
	Enemy_RatBite,
	Enemy_WolfGotHit,
	Enemy_WolfDeath,
	Enemy_WolfBite,
	Knight_GotHit,
	Knight_Death,
	Knight_Swing,
	Knight_HealthUp,
	Knight_Catch,
	Weapon_SwordHit_Flesh,
	General_ThrowItem,
	General_PickupItem,
	General_PutdownItem,
	General_TrashItem,
	General_FailDropItem,
	General_RotateItem,
	Knight_LowHealth,
}

public class AudioManager : MonoSingleton<AudioManager> {

	public bool m_Disabled;

	private Dictionary<AudioEvent,AudioCollection> m_AudioCollections = new Dictionary<AudioEvent, AudioCollection>();

	public void Awake() {
		var AudioCollections = GetComponents<AudioCollection> ();
		int i;
		for (i=0; i<AudioCollections.Length; i++) {
			if ( m_AudioCollections.ContainsKey(AudioCollections [i].m_AudioEvent) ) {
				Debug.LogError("AudioManager Awake found duplicate AudioCollection");
				continue;
			}
			m_AudioCollections.Add (AudioCollections [i].m_AudioEvent, AudioCollections [i]);
		}
	}

	public int GetAudioSourceIndex( AudioEvent audioEvent ) {
		if (!m_AudioCollections.ContainsKey (audioEvent)) {
			Debug.LogError("AudioManager GetAudioSourceIndex() - " + audioEvent + " missing Collection" );
			return -1;
		}
		return m_AudioCollections [audioEvent].GetAudioSourceIndex ();
	}

	public float GetAudioSourceLength( AudioEvent audioEvent, int audioSourceIndex ) {
		if (!m_AudioCollections.ContainsKey (audioEvent)) {
			Debug.LogError("AudioManager GetAudioSourceLength() - " + audioEvent + " missing Collection" );
			return 0f;
		}
		var prefab = m_AudioCollections [audioEvent].GetAudioSourcePrefab (audioSourceIndex);
		if (prefab == null) {
			Debug.Log ("GetAudioSourceLength() for " + audioEvent.ToString() + " Index:" + audioSourceIndex + " null prefab" );
			return 0f;
		}
		return prefab.clip.length;
	}

	public GameObject SpawnAudio(AudioEvent audioEvent, int audioSourceIndex, Vector3 position, Quaternion rotation) {

		if (!m_AudioCollections.ContainsKey (audioEvent)) {
			Debug.LogError("AudioManager GetAudioSourceLength() - " + audioEvent + " missing Collection" );
			return null;
		}
		var prefab = m_AudioCollections [audioEvent].GetAudioSourcePrefab (audioSourceIndex);
		if (prefab == null) {
			Debug.Log ("GetAudioSourceLength() for " + audioEvent.ToString() + " Index:" + audioSourceIndex + " null prefab" );
			return null;
		}

		var spawned = PoolManager.Pools ["AudioEvents"].Spawn (prefab, position, rotation);

		if (m_Disabled || m_AudioCollections [audioEvent].m_Disable) {
			spawned.gameObject.GetComponent<AudioSource> ().Stop ();
		}
		return spawned.gameObject;
	}

	public GameObject SpawnAudio(AudioEvent audioEvent, Vector3 position, Quaternion rotation) {
		return SpawnAudio(audioEvent, GetAudioSourceIndex(audioEvent), position, rotation);
	}

	private void Update() {
		if(Input.GetKeyDown(KeyCode.M)) {
			m_Disabled = !m_Disabled;
		}
	}
}
