﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Quest))]
public class QuestInspector : Editor 
{
	public override void OnInspectorGUI()
	{
		Quest myTarget = (Quest)target;

		//myTarget.experience = EditorGUILayout.IntField("Experience", myTarget.experience);
		EditorGUILayout.LabelField("Knight Health After Quest", myTarget.KnightHealthAfterQuest().ToString() );

		// Show default inspector property editor
		DrawDefaultInspector ();
	}
}