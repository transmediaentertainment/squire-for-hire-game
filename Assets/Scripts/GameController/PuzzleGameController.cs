﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class PuzzleGameController : MonoBehaviour {

	private Inventory m_Inventory;
	private Quest m_Quest;

	public event Action<int> ValueUpdated;
	public Material m_Mat1;
	public Material m_Mat2;
	public Material m_HoverValidMat;
	public Material m_HoverInvalidMat;
	public Material m_TrashMat;
	public Material m_TempMat;
	public Material m_ThrowMat;

	public GameObject m_CircleTarget;
	private readonly Vector3 m_CircleUpperScale = new Vector3(4f, 4f, 4f);
	private readonly Vector3 m_CircleLowerScale = new Vector3(0.5f, 0.5f, 0.5f);
	private readonly Color m_CircleValidColor = Color.green;
	private readonly Color m_CircleInvalidColor = Color.red;
	private readonly float m_StartDist = 5f;
	private readonly float m_WarnDist = 2f;
	private readonly float m_CatchDistance = 1f;
	private float m_CurrentDist;
	private ThrownItemController m_ThrownToSquireItem;

	private GameObject m_CurrentPickupObject;

	private const float AfterKnightFinishTime = 15f;
	private float m_FinalCountdown = 0f;

	public event Action<string> QuestFailed;
	public event Action<string> QuestSucceeded;

	public InputController m_Input;

	public GameObject m_EnemyPrefab;
	public GameObject m_KnightPrefab;
	public GameObject m_SquirePrefab;
	public GameObject m_HealthPotionPrefab;
	public GameObject m_SwordPrefab;
	public GameObject m_AxePrefab;
	
	public GameObject m_DroppedItemPrefab;
	
	private EnemyController[] m_SpawnedEnemies;
	private List<GameObject> m_DroppedItems = new List<GameObject>();
	private int m_CurrentEnemy = 0;
	private KnightController m_SpawnedKnight;
	private SquireController m_SpawnedSquire;
	
	public event Action<DroppedItem> ItemDroppedByEnemy;
	public event Action<float> TimeRemainingUpdated;
	public event Action<int, int> KnightsHealthChanged;

	public event Action<Item> SquireDroppedThrownItem;
	public event Action<Item> SquireCaughtThrownItem;

	public ThirdPersonCam m_ThirdPersonCam;
	public GameObject m_PathPrefab;
	private List<GameObject> m_PathSections = new List<GameObject>();

	public MovingUICameraController m_MovingUICamera;

	private void Awake() {
		m_CircleTarget.SetActive (false);
		m_Input.ItemThrownFromSquireToKnight += HandleItemThrownFromSquireToKnight;
	}

	void HandleItemThrownFromSquireToKnight (Item item) {
		var throwGO = GameObject.CreatePrimitive (PrimitiveType.Quad);
		throwGO.transform.localScale = Vector3.one * 0.3f;
		throwGO.name = "Thrown Object";
		var throwController = throwGO.AddComponent<ThrownItemController> ();
		throwController.Init (item, 3f, m_SpawnedKnight, m_SpawnedSquire, true);
		AudioManager.Instance.SpawnAudio (AudioEvent.General_ThrowItem, throwGO.transform.position, throwGO.transform.rotation);
	}
	
	void HandleWeaponThrownFromKnightToSquire (WeaponItem weapon)
	{
		var throwGO = GameObject.CreatePrimitive (PrimitiveType.Quad);
		throwGO.transform.localScale = Vector3.one * 0.3f;
		throwGO.name = "Thrown Object";
		var throwController = throwGO.AddComponent<ThrownItemController> ();
		throwController.Init (weapon, 3f, m_SpawnedKnight, m_SpawnedSquire, false);
		throwController.WentPastSquire += HandleWentPastSquire;
		BeginCatchRoutine (throwController);
	}

	void HandleWentPastSquire ()
	{
		MissedCatch ();
		FinishCatchRoutine ();
	}

	public void StartNewGame(Quest quest) {
		m_Quest = quest;
		var go = new GameObject ("Inventory", typeof(Inventory));
		m_Inventory = go.GetComponent<Inventory> ();
		m_Inventory.InventoryChanged += HandleInventoryChanged;
		m_Inventory.Init (m_MovingUICamera.GetComponent<Camera>(), m_CircleTarget.transform, m_Mat1, m_Mat2, m_HoverValidMat, m_HoverInvalidMat, m_TrashMat, m_TempMat, m_ThrowMat);


		// TODO : Remove this once a persistant Inventory system is working.
		for (int i = 0; i < 5; i++) {
			var potion = GameObject.Instantiate(m_HealthPotionPrefab);
			potion.transform.position = Inventory.GetWorldGridPos(i, 0);
			var item = potion.GetComponent<Item>();
			item.Init(m_Inventory, m_Quest);
			item.SpawnedInInventory();
			m_Inventory.AddItem(item, 0f, i, 0);
		}

		var sword = GameObject.Instantiate (m_SwordPrefab);
		sword.transform.position = Inventory.GetWorldGridPos (0, 1);
		var swordItem = sword.GetComponent<Item> ();
		swordItem.Init (m_Inventory, m_Quest);
		swordItem.SpawnedInInventory ();
		m_Inventory.AddItem (swordItem, 0f, 0, 1);

		var axe = GameObject.Instantiate (m_AxePrefab);
		axe.transform.position = Inventory.GetWorldGridPos (1, 1);
		var axeItem = axe.GetComponent<Item> ();
		axeItem.Init (m_Inventory, m_Quest);
		axeItem.SpawnedInInventory ();
		m_Inventory.AddItem (axeItem, 0f, 1, 1);

		// end TODO Removal

		SetupUpperAction (m_Quest);
	}

	private void BeginCatchRoutine(ThrownItemController thrownItem) {
		m_ThrownToSquireItem = thrownItem;
		SystemEventCenter.Instance.LateUpdateEvent += CatchRoutineUpdate;
		m_Input.ThrowIconClicked += HandleThrowIconClicked;
	}

	private void FinishCatchRoutine() {
		GameObject.Destroy (m_ThrownToSquireItem.gameObject);
		SystemEventCenter.Instance.LateUpdateEvent -= CatchRoutineUpdate;
		m_Input.ThrowIconClicked -= HandleThrowIconClicked;
		m_CircleTarget.SetActive (false);
	}

	public bool IsPickupItemQuestItem {
		get {
			if(m_CurrentPickupObject == null) {
				return false;
			}
			return m_CurrentPickupObject.GetComponent<Item>().IsQuestItem;
		}
	}

	void HandleThrowIconClicked ()
	{
		if (m_CurrentDist < m_CatchDistance) {
			// Caught Weapon
			var caughtItem = m_ThrownToSquireItem.GetItem();
			caughtItem.CaughtBySquire();
			m_Input.ForceReplaceCarryingItem(caughtItem);
		} else {
			MissedCatch ();
		}
		FinishCatchRoutine ();
	}

	void MissedCatch ()
	{

		AudioManager.Instance.SpawnAudio (AudioEvent.General_FailDropItem, transform.position, transform.rotation);

		// Missed Catch
		var droppedItemGO = GameObject.Instantiate (m_DroppedItemPrefab);
		var droppedItem = droppedItemGO.GetComponent<DroppedItem> ();
		droppedItem.Init (m_ThrownToSquireItem.GetItem ().gameObject);
		// Find next valid item and put it after that
		int index = -1;
		for (int i = 0; i < m_DroppedItems.Count; i++) {
			if (m_DroppedItems [i].activeInHierarchy) {
				index = i;
				break;
			}
		}
		if (index == -1) {
			// Didn't find one, just put it 1 unit ahead of player
			droppedItem.transform.position = m_SpawnedSquire.transform.position + new Vector3 (1f, 0f, 0f);
			m_DroppedItems.Add (droppedItemGO);
		}
		else {
			droppedItem.transform.position = m_DroppedItems [index].transform.position + new Vector3 (1f, 0f, 0f);
			m_DroppedItems.Insert (index + 1, droppedItemGO);
		}
		droppedItem.ItemPickedUp += HandleItemPickedUp;
		m_SpawnedSquire.HandleWeaponDropped (droppedItem);
	}

	void CatchRoutineUpdate (float deltaTime)
	{
		m_CurrentDist = (m_ThrownToSquireItem.transform.position - m_SpawnedSquire.transform.position).magnitude;
		var sprite = m_CircleTarget.GetComponent<SpriteRenderer> ();

		if (m_CurrentDist <= m_StartDist) {
			m_CircleTarget.SetActive(true);
			m_CircleTarget.transform.localScale = Vector3.Lerp(m_CircleUpperScale, m_CircleLowerScale, 1f - (m_CurrentDist / m_StartDist));
			if(m_CurrentDist > m_WarnDist) {
				sprite.color = m_CircleInvalidColor;
			} else if(m_CurrentDist < m_CatchDistance) {
				sprite.color = m_CircleValidColor;
			} else {
				var diff = m_WarnDist - m_CatchDistance;
				var point = m_CurrentDist - m_CatchDistance;
				sprite.color = Color.Lerp(m_CircleValidColor, m_CircleInvalidColor, (point / diff));
			}
		}
	}

	void FinalCountdownUpdate (float deltaTime)
	{
		m_FinalCountdown += deltaTime;
		if (TimeRemainingUpdated != null) {
			TimeRemainingUpdated(AfterKnightFinishTime - m_FinalCountdown);
		}
		if (m_FinalCountdown >= AfterKnightFinishTime) {
			m_SpawnedSquire.SprintToEnd();
			SystemEventCenter.Instance.UpdateEvent -= FinalCountdownUpdate;
		}
	}

	private void Finish() {
		var required = m_Quest.GetRequiredQuestItems ();
		if (required == null) {
			// We finished the quest
			if (QuestSucceeded != null) {
				QuestSucceeded("You made it to town!");
			}
		} else {
			if(m_Inventory.ContainsItemsWithNames(required)) {
				// Got all quest items
				// We finished the quest
				if (QuestSucceeded != null) {
					QuestSucceeded("You made it and collected all the required items!");
				}
			} else {
				// Missed some quest items
				if(QuestFailed != null) {
					QuestFailed("You didn't collect all the required items. Someone won't be happy about that.");
				}
			}
		}
	}

	void HandleItemPickedUp (GameObject obj)
	{
		m_CurrentPickupObject = obj;
		m_CurrentPickupObject.GetComponent<Item> ().Init (m_Inventory, m_Quest);
		RegisterItemEvents (m_CurrentPickupObject);
	}

	private void RegisterItemEvents(GameObject go) {
		var item = go.GetComponent<Item> ();
		item.RemovedFromPickupArea += HandleRemovedFromPickupArea;
	}

	private void CleanItemEvents(GameObject go) {
		var item = go.GetComponent<Item> ();
		item.RemovedFromPickupArea -= HandleRemovedFromPickupArea;
	}

	void HandleRemovedFromPickupArea ()
	{
		CleanItemEvents (m_CurrentPickupObject);
		m_CurrentPickupObject = null;
	}

	void HandleInventoryChanged ()
	{
		if (ValueUpdated != null) {
			ValueUpdated(m_Inventory.GetInventoryValue());
		}
	}

	public void FinishGame() {

		if ( m_SpawnedKnightLowHealthAudioObject != null ) {
			if ( m_SpawnedKnightLowHealthAudioObject.GetComponent<AudioSource> ().isPlaying ) {
				m_SpawnedKnightLowHealthAudioObject.GetComponent<AudioSource> ().Stop();
				m_SpawnedKnightLowHealthAudioObject = null;
			}
		}

		m_Inventory.InventoryChanged -= HandleInventoryChanged;
		var tempStore = m_Inventory.TakeItemFromTempStore ();
		if (tempStore != null) {
			Destroy(tempStore.gameObject);
		}
		Destroy (m_Inventory.gameObject);

		if (m_CurrentPickupObject != null) {
			Destroy(m_CurrentPickupObject);
		}

		m_CircleTarget.SetActive (false);

		SystemEventCenter.Instance.UpdateEvent -= FinalCountdownUpdate;

		CleanUpperAction ();

		m_Input.CleanUp ();

		ThrownItemController.CleanThrownItems ();

		Destroy (m_Quest.gameObject);
		m_Quest = null;

		m_MovingUICamera.CleanUp ();
	}

	private void SetupUpperAction(Quest quest) {

		m_SpawnedEnemies = quest.Init ();
		
		var pos = new Vector3 (0f, 0.25f, quest.m_StartDist);
		for (int i = 0; i < m_SpawnedEnemies.Length; i++) {
			m_SpawnedEnemies[i].transform.position = pos;
			pos.z += UnityEngine.Random.Range(quest.m_EnemyDistanceMin, quest.m_EnemyDistanceMax);
		}
		
		pos.z += quest.m_EndDist;

		var pathPos = -5f;
		while (pathPos < pos.z) {
			var path = (GameObject)GameObject.Instantiate(m_PathPrefab, new Vector3(0f, 0f, pathPos), Quaternion.identity);
			m_PathSections.Add(path);
			pathPos += 1f;
		}


		m_Quest = quest;


		var knightGO = (GameObject)GameObject.Instantiate (m_KnightPrefab.gameObject, new Vector3(0f, 0.5f, 0f), Quaternion.identity);
		m_SpawnedKnight = knightGO.GetComponent<KnightController> ();
		m_SpawnedKnight.Init (pos);
		m_SpawnedKnight.ReachedEnd += HandleKnightReachedEnd;
		m_SpawnedKnight.HealthChanged += HandleKnightHealthChanged;
		m_SpawnedKnight.KnightDied += HandleKnightDied;
		m_SpawnedKnight.WeaponThrown += HandleWeaponThrownFromKnightToSquire;
		if (KnightsHealthChanged != null) {
			KnightsHealthChanged(m_SpawnedKnight.m_Health, m_SpawnedKnight.m_MaxHealth);
		}

		var squireGO = (GameObject)GameObject.Instantiate (m_SquirePrefab.gameObject, new Vector3 (0f, 0.5f, -2f), Quaternion.identity);
		m_SpawnedSquire = squireGO.GetComponent<SquireController> ();
		m_SpawnedSquire.Init (m_Input, m_SpawnedKnight, this, pos);
		m_SpawnedSquire.ReachedEnd += HandleSquireReachedEnd;
		m_SpawnedSquire.ClosedBag += HandleClosedBag;
		
		m_CurrentEnemy = 0;
		m_SpawnedEnemies[m_CurrentEnemy].EnemyDied += HandleEnemyDied;
		m_SpawnedKnight.SetNextEnemy (m_SpawnedEnemies [0]);

		m_ThirdPersonCam.Init (m_SpawnedSquire.transform);

		m_MovingUICamera.Init (m_SpawnedSquire, m_SpawnedKnight);
	}

	void HandleClosedBag ()
	{
		if (m_CurrentPickupObject != null) {
			CleanItemEvents(m_CurrentPickupObject);
			m_CurrentPickupObject.GetComponent<Item>().Trashed();
			m_CurrentPickupObject = null;
		}
	}

	void HandleKnightDied ()
	{
		if (QuestFailed != null) {
			QuestFailed("You let the knight die!  Now who will you squire for?");
		}
	}

	private GameObject m_SpawnedKnightLowHealthAudioObject = null;
	private const float c_LowHealthPercentage = 0.4f;

	void HandleKnightHealthChanged (int health, int maxHealth)
	{
		if (health < maxHealth * c_LowHealthPercentage) {
			// low health
			if (m_SpawnedKnightLowHealthAudioObject == null) { 
				// start the sound on loop
				m_SpawnedKnightLowHealthAudioObject = AudioManager.Instance.SpawnAudio (AudioEvent.Knight_LowHealth, transform.position, transform.rotation);
				m_SpawnedKnightLowHealthAudioObject.GetComponent<AudioSource> ().loop = true;
			} else {
				if ( !m_SpawnedKnightLowHealthAudioObject.GetComponent<AudioSource> ().isPlaying ) {
					m_SpawnedKnightLowHealthAudioObject.GetComponent<AudioSource> ().Play();
				}
			}
		} else {
			// good health
			if ( m_SpawnedKnightLowHealthAudioObject != null ) {
				if ( m_SpawnedKnightLowHealthAudioObject.GetComponent<AudioSource> ().isPlaying ) {
					m_SpawnedKnightLowHealthAudioObject.GetComponent<AudioSource> ().Stop();
					m_SpawnedKnightLowHealthAudioObject = null;
				}
			}
		}

		if (KnightsHealthChanged != null) {
			KnightsHealthChanged(health, maxHealth);
		}
	}
	
	void HandleSquireReachedEnd ()
	{
		Finish ();
	}
	
	void HandleKnightReachedEnd ()
	{
		// Begin Final Countdown
		m_FinalCountdown = 0f;
		SystemEventCenter.Instance.UpdateEvent += FinalCountdownUpdate;
	}
	
	void HandleEnemyDied (Item itemPrefab)
	{
		var droppedItemGO = GameObject.Instantiate (m_DroppedItemPrefab);
		droppedItemGO.name.RemoveClone ();
		var droppedItem = droppedItemGO.GetComponent<DroppedItem> ();
		var item = GameObject.Instantiate (itemPrefab.gameObject);
		item.name = item.name.RemoveClone ();
		droppedItem.Init (item);
		droppedItemGO.transform.position = m_SpawnedEnemies [m_CurrentEnemy].transform.position;
		m_DroppedItems.Add (droppedItemGO);
		if (ItemDroppedByEnemy != null) {
			ItemDroppedByEnemy(droppedItem);

		}
		droppedItem.ItemPickedUp += HandleItemPickedUp;
		m_SpawnedEnemies[m_CurrentEnemy].EnemyDied -= HandleEnemyDied;
		m_CurrentEnemy++;
		if (m_CurrentEnemy >= m_SpawnedEnemies.Length) {
			// Finished
			m_SpawnedKnight.SetOffScreenTarget ();
			m_SpawnedSquire.SetToFinishing();
		} else {
			m_SpawnedKnight.SetNextEnemy(m_SpawnedEnemies[m_CurrentEnemy]);
			m_SpawnedEnemies[m_CurrentEnemy].EnemyDied += HandleEnemyDied;
		}
	}
	
	public void CleanUpperAction() {
		for(int i = 0; i < m_SpawnedEnemies.Length; i++) {
			Destroy(m_SpawnedEnemies[i].gameObject);
		}
		m_SpawnedEnemies = null;
		for (int i = 0; i < m_DroppedItems.Count; i++) {
			Destroy (m_DroppedItems[i]);
		}
		m_DroppedItems.Clear ();
		for (int i = 0; i < m_PathSections.Count; i++) {
			Destroy(m_PathSections[i]);
		}
		m_PathSections.Clear ();

		m_ThirdPersonCam.CleanUp ();
		Destroy (m_SpawnedSquire.gameObject);
		m_SpawnedKnight.CleanUp ();
		Destroy (m_SpawnedKnight.gameObject);
	}

	public int GetScore() {
		return m_Inventory.GetInventoryValue ();
	}
}
