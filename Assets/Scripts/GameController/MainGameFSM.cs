﻿using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;
using UnityEngine.UI;

public class MainGameFSM : FSMBehaviour {

	private MainMenuState m_MainMenuState;
	private TownState m_TownState;
	private PuzzleGameState m_PuzzleGameState;
	private QuestReportState m_QuestReportState;

	public PuzzleGameController m_PuzzleGameController;
	public UIFSM m_UIFSM;

	public Quest[] m_Quests;

	void Start() {
		m_MainMenuState = new MainMenuState (m_UIFSM);
		m_TownState = new TownState (m_UIFSM);
		m_PuzzleGameState = new PuzzleGameState (this, m_PuzzleGameController, m_UIFSM);
		m_QuestReportState = new QuestReportState ( m_UIFSM);

		NewState (m_MainMenuState);
	}

	public void NewGame() {
		NewState (m_TownState, m_Quests);
	}

	public void StartQuest(Quest quest) {
		var questInstance = GameObject.Instantiate (quest.gameObject).GetComponent<Quest>();
		NewState (m_PuzzleGameState, questInstance);
	}

	public void ReturnToTown() {
		NewState (m_TownState, m_Quests);
	}

	public void CompleteQuest(bool success, string message) {
		NewState (m_QuestReportState, success, message);
	}
}
