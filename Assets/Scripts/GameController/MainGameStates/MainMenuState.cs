﻿using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;

public class MainMenuState : FSMState {

	private UIFSM m_UIFSM;

	public MainMenuState(UIFSM uifsm) {
		m_UIFSM = uifsm;
	}

	public override void Enter (params object[] args)
	{
		base.Enter (args);
		m_UIFSM.ShowMainMenu ();
	}
}
