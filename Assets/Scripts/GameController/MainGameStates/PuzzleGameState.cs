﻿using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;

public class PuzzleGameState : FSMState {

	private Quest m_CurrentQuest;
	private MainGameFSM m_FSM;
	private PuzzleGameController m_PGC;
	private UIFSM m_UIFSM;

	public PuzzleGameState(MainGameFSM fsm, PuzzleGameController pgc, UIFSM uifsm) {
		m_FSM = fsm;
		m_PGC = pgc;	
		m_UIFSM = uifsm;
	}

	public override void Enter (params object[] args)
	{
		base.Enter (args);
		if (args != null && args.Length == 1) {
			m_CurrentQuest = (Quest)args [0];
		} else {
			Debug.LogError("No Quest passed into Puzzle Game State");
		}
		m_UIFSM.ShowPuzzleGameUI ();
		m_PGC.StartNewGame (m_CurrentQuest);
		m_PGC.QuestSucceeded += HandleQuestSucceeded;
		m_PGC.QuestFailed += HandleQuestFailed;
	}

	void HandleQuestFailed (string message)
	{
		m_FSM.CompleteQuest (false, message + "\nBut you still collected $" + m_PGC.GetScore());
	}

	void HandleQuestSucceeded (string message)
	{
		m_FSM.CompleteQuest (true, message + "\nAnd you collected $" + m_PGC.GetScore());
	}

	public override void Exit ()
	{
		base.Exit ();

		m_PGC.QuestSucceeded -= HandleQuestSucceeded;
		m_PGC.QuestFailed -= HandleQuestFailed;
		m_CurrentQuest = null;
		m_PGC.FinishGame ();
	}
}
