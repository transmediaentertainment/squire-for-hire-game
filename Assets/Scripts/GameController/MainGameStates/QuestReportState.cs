﻿using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;

public class QuestReportState : FSMState {

	private UIFSM m_UIFSM;

	public QuestReportState(UIFSM uifsm) {
		m_UIFSM = uifsm;
	}

	public override void Enter (params object[] args)
	{
		base.Enter (args);

		if (args == null || args.Length != 2) {
			Debug.LogError("Incorrect data passed into QuestReportState");
			return;
		}

		var success = (bool)args [0];
		var message = (string)args [1];

		m_UIFSM.ShowQuestReportUI (success, message);
	}
}
