﻿using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;

public class TownState : FSMState {

	private UIFSM m_UIFSM;

	public TownState(UIFSM uifsm) {
		m_UIFSM = uifsm;
	}

	public override void Enter (params object[] args)
	{
		base.Enter (args);
		if (args == null || args.Length == 0) {
			Debug.LogError("Invalid args passed into Town State");
			return;
		}
		var quests = (Quest[])args;
		m_UIFSM.ShowTownUI (quests);
	}

}
