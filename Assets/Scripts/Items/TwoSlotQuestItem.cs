﻿using UnityEngine;
using System.Collections;

public class TwoSlotQuestItem : Item {
	
	protected override void Awake ()
	{
		base.Awake ();
		m_SlotsFilled = new bool[1, 2];
		m_SlotsFilled [0, 0] = true;
		m_SlotsFilled [0, 1] = true; 
	}
}
