﻿using UnityEngine;
using System.Collections;

public class FourSlotSquareItem : Item {

	// Use this for initialization
	protected override void Awake ()
	{
		base.Awake ();
		m_SlotsFilled = new bool[2, 2];
		m_SlotsFilled [0, 0] = true;
		m_SlotsFilled [1, 0] = true;
		m_SlotsFilled [0, 1] = true;
		m_SlotsFilled [1, 1] = true;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
