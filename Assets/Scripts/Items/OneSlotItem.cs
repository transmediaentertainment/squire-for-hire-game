﻿using UnityEngine;
using System.Collections;

public class OneSlotItem : Item {

	protected override void Awake ()
	{
		base.Awake ();
		m_SlotsFilled = new bool[1, 1];
		m_SlotsFilled [0, 0] = true;
	}
}
