﻿using UnityEngine;
using System.Collections;

public class HealthPotion : OneSlotItem {

	public int m_HealthAmount = 10;

	protected override void Awake ()
	{
		base.Awake ();

		KnightReceiveBehaviour = (knight) => {
			knight.ApplyHealing(m_HealthAmount);
		};
	}
}
