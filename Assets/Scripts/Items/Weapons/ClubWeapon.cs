﻿using UnityEngine;
using System.Collections;

public class ClubWeapon : WeaponItem {

	protected override void Awake ()
	{
		base.Awake ();

		m_WeaponType = WeaponType.Club;

		m_SlotsFilled = new bool[1, 3];
		for(int i = 0; i < 3; i++) {
			m_SlotsFilled[0,i] = true;
		}
	}
}
