﻿using UnityEngine;
using System.Collections;

public class SpearWeapon : WeaponItem {

	protected override void Awake ()
	{
		base.Awake ();

		m_WeaponType = WeaponType.Spear;

		m_SlotsFilled = new bool[1, 4];
		for(int i = 0; i < 4; i++) {
			m_SlotsFilled[0,i] = true;
		}
	}
}
