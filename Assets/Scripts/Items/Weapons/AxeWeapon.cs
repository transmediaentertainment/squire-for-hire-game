﻿using UnityEngine;
using System.Collections;

public class AxeWeapon : WeaponItem {

	protected override void Awake ()
	{
		base.Awake ();

		m_WeaponType = WeaponType.Axe;

		m_SlotsFilled = new bool[2, 3];
		m_SlotsFilled [0, 0] = true;
		m_SlotsFilled [1, 0] = false;
		m_SlotsFilled [0, 1] = true;
		m_SlotsFilled [1, 1] = false;
		m_SlotsFilled [0, 2] = true;
		m_SlotsFilled [1, 2] = true;
	}
}
