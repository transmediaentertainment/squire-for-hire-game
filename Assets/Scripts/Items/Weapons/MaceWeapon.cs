﻿using UnityEngine;
using System.Collections;

public class MaceWeapon : WeaponItem {

	protected override void Awake ()
	{
		base.Awake ();

		m_WeaponType = WeaponType.Mace;

		m_SlotsFilled = new bool[1, 3];
		for(int i = 0; i < 3; i++) {
			m_SlotsFilled[0,i] = true;
		}
	}
}
