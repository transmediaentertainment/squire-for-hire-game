﻿using UnityEngine;
using System.Collections;

public enum WeaponType {
	Sword,
	Axe,
	Mace,
	Club,
	Spear
}

public abstract class WeaponItem : Item {

	protected WeaponType m_WeaponType;
	public WeaponType WeaponType { get { return m_WeaponType; } }

	protected override void Awake ()
	{
		base.Awake ();

		m_IsThrowable = true;

		KnightReceiveBehaviour = (knight) => {
			knight.CatchWeapon(this);
		};

		SquireReceiveBehaviour = (squire) => {

		};
	}
}
