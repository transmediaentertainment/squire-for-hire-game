﻿using UnityEngine;
using System.Collections;

public class Item_3x2 : Item {

	// Use this for initialization
	protected override void Awake() {
		base.Awake ();
		m_SlotsFilled = new bool[3, 2];
		m_SlotsFilled [0, 0] = true;
		m_SlotsFilled [1, 0] = true;
		m_SlotsFilled [2, 0] = true;
		m_SlotsFilled [0, 1] = true;
		m_SlotsFilled [1, 1] = true;
		m_SlotsFilled [2, 1] = true;
	}
	
}
