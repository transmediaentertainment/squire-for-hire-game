using UnityEngine;
using System.Collections;
using System;

public enum ItemState {
	None,
	Dropped,
	IdleOutside,
	PickedUp,
	IdleInside,
	IdleInTempStore,
	Thrown,
	KnightCarrying
}

public abstract class Item : MonoBehaviour {

	public bool[,] m_SlotsFilled;
	public Material m_Material;
	private GameObject[,] m_Panels;
	private GameObject[,] m_BackingPanels;

	private ItemState m_CurrentState = ItemState.None;
	private ItemState m_LastState = ItemState.None;
	private InvPos m_LastInvPos;

	private Vector3 m_StartPoint;

	private float m_Rotation = 0f;
	public int m_Value;

	private Inventory m_Inventory;
	private Quest m_Quest;

	public event Action RemovedFromPickupArea;

	public bool m_IsThrowable = false;
	public bool m_IsJunk = true;

	protected Action<KnightController> KnightReceiveBehaviour;
	protected Action<SquireController> SquireReceiveBehaviour;

	public void Init(Inventory inv, Quest quest) {
		m_Inventory = inv;
		m_Quest = quest;
	}

	public void SpawnedInInventory() {
		SetNewState (ItemState.IdleInside);
	}

	protected virtual void Awake() {
	}

	public bool IsQuestItem {
		get {
			if(m_Quest == null) {
				return false;
			}
			return m_Quest.IsQuestItem(this);
		}
	}

	void Start() {
		int width = m_SlotsFilled.GetLength (0);
		int height = m_SlotsFilled.GetLength (1);
		m_Panels = new GameObject[width, height];
		m_BackingPanels = new GameObject[width, height];
		Material backingMaterial = Resources.Load ("Materials/ItemBacking") as Material;

		for (int x = 0; x < width; x++) {
			for(int y = 0; y < height; y++) {
				if(m_SlotsFilled[x,y]) {
					var panel = GameObject.CreatePrimitive(PrimitiveType.Quad);
					panel.transform.parent = transform;
					panel.transform.localPosition = new Vector3(x * Inventory.SquareScale, y * Inventory.SquareScale, 0);
					panel.transform.localScale = new Vector3(Inventory.SquareScale, Inventory.SquareScale, 1f);
					panel.GetComponent<Renderer>().sharedMaterial = m_Material;
					panel.layer = LayerMask.NameToLayer("Item");
					var meshFilter = panel.GetComponent<MeshFilter>();
					meshFilter.mesh.uv = MeshUtilities.GetUVsForPositionedQuad(x, y, width, height, meshFilter.mesh);
					m_Panels[x,y] = panel;

					var backingPanel = GameObject.CreatePrimitive(PrimitiveType.Quad);
					backingPanel.GetComponent<Renderer>().sharedMaterial = backingMaterial;
					backingPanel.layer = LayerMask.NameToLayer("MovingUI");
					backingPanel.transform.parent = transform;
					backingPanel.transform.localPosition = new Vector3(x * Inventory.SquareScale, y * Inventory.SquareScale, 0.025f);
					backingPanel.transform.localScale = new Vector3(Inventory.SquareScale, Inventory.SquareScale, 1f) * 0.95f;
					m_BackingPanels[x,y] = backingPanel;
				}
			}
		}

		if (m_CurrentState == ItemState.None) {
			m_CurrentState = ItemState.IdleOutside;
			m_LastState = ItemState.None;
		}
	}

	public void CleanUp() {
		SystemEventCenter.Instance.LateUpdateEvent -= HoverLateUpdate;
	}

	public void PickedUpInLowerSection() {
		if (m_CurrentState == ItemState.IdleInside) {
			m_LastInvPos = m_Inventory.RemoveItem (this);
			if (m_LastInvPos == null) {
				Debug.LogError ("Didn't find item in inventory");
			}
		} else if (m_CurrentState == ItemState.IdleInTempStore) {
			m_Inventory.TakeItemFromTempStore();
		}
		m_StartPoint = transform.position;

		AudioManager.Instance.SpawnAudio (AudioEvent.General_PickupItem, transform.position, transform.rotation);

		SetNewState (ItemState.PickedUp);
	}

	public void PickedUpFromDroppedItems() {
		SetNewState (ItemState.IdleOutside);
	}

	private void SetNewState(ItemState state) {
		if (state == ItemState.PickedUp && m_CurrentState != ItemState.PickedUp) {
			SystemEventCenter.Instance.LateUpdateEvent += HoverLateUpdate;
		} else if (m_CurrentState == ItemState.PickedUp && state != ItemState.PickedUp) {
			SystemEventCenter.Instance.LateUpdateEvent -= HoverLateUpdate;
		}
		m_LastState = m_CurrentState;
		m_CurrentState = state;
	}

	private void HoverLateUpdate(float deltaTime) {
		m_Inventory.ClearHovered ();
		int snapX = Inventory.WorldToGridX (transform.position.x);
		int snapY = Inventory.WorldToGridY (transform.position.y);
		for (var x = 0; x < m_SlotsFilled.GetLength(0); x++) {
			for(var y = 0; y < m_SlotsFilled.GetLength(1); y++) {
				if(m_SlotsFilled[x,y]) {
					var adjustedX = x;
					var adjustedY = y;
					if(m_Rotation.IsApproximately(90f)) {
						adjustedX = -y;
						adjustedY = x;
						
					} else if(m_Rotation.IsApproximately(180f)) {
						adjustedX = -x;
						adjustedY = -y;
						
					} else if(m_Rotation.IsApproximately(270f)) {
						adjustedX = y;
						adjustedY = -x;
					}
					m_Inventory.SetHovered(adjustedX + snapX, adjustedY + snapY);
				}
			}
		}
	}

	public void DroppedInLowerSection() {
		// Snap position
		int snapX = Inventory.WorldToGridX (transform.position.x);
		int snapY = Inventory.WorldToGridY (transform.position.y);
		bool failed = false;
		for (var x = 0; x < m_SlotsFilled.GetLength(0); x++) {
			for(var y = 0; y < m_SlotsFilled.GetLength(1); y++) {
				if(m_SlotsFilled[x,y]) {
					var adjustedX = x;
					var adjustedY = y;
					if(m_Rotation.IsApproximately(90f)) {
						adjustedX = -y;
						adjustedY = x;

					} else if(m_Rotation.IsApproximately(180f)) {
						adjustedX = -x;
						adjustedY = -y;

					} else if(m_Rotation.IsApproximately(270f)) {
						adjustedX = y;
						adjustedY = -x;
					}
					if(!m_Inventory.IsSlotInRangeAndEmpty(adjustedX + snapX, adjustedY + snapY)) {
						failed = true;
						break;
					}
				}
			}
			if(failed) {
				break;
			}
		}

		AudioManager.Instance.SpawnAudio (AudioEvent.General_PutdownItem, transform.position, transform.rotation);

		if (failed) {
			RevertToLastState ();

			AudioManager.Instance.SpawnAudio (AudioEvent.General_FailDropItem, transform.position, transform.rotation);

		} else {
			if(m_LastState == ItemState.IdleOutside) {
				if(RemovedFromPickupArea != null) {
					RemovedFromPickupArea();
				}
			}
			transform.position = Inventory.GetWorldGridPos(snapX, snapY) + new Vector3(0f, 0f, m_StartPoint.z);
			m_Inventory.AddItem(this, m_Rotation, snapX, snapY);
			SetNewState(ItemState.IdleInside);
		}

		m_Inventory.ClearHovered ();
	}

	public void RevertToLastState ()
	{
		if (m_LastState == ItemState.IdleInside && m_LastInvPos != null) {
			m_Inventory.AddItem (m_LastInvPos);
			m_Rotation = m_LastInvPos.Rotation;
			Rotate ();
		}
		transform.position = m_StartPoint;
		SetNewState (m_LastState);
	}

	public void Trashed() {
		if (m_Quest.IsQuestItem (this)) {
			RevertToLastState();
			return;
		}
		if (m_CurrentState == ItemState.IdleInside) {
			m_LastInvPos = m_Inventory.RemoveItem (this);
			m_LastInvPos = null;
		} else if (m_LastState == ItemState.IdleOutside) {
			if(RemovedFromPickupArea != null) {
				RemovedFromPickupArea();
			}
		}

		AudioManager.Instance.SpawnAudio (AudioEvent.General_TrashItem, transform.position, transform.rotation);

		CleanUp ();
		Destroy (gameObject);
	}

	public void Thrown() {
		if (m_LastState == ItemState.IdleOutside) {
			if(RemovedFromPickupArea != null) {
				RemovedFromPickupArea();
			}
		}
		SetNewState (ItemState.Thrown);
		gameObject.SetActive (false);
	}

	public void CaughtBySquire() {
		SetNewState (ItemState.PickedUp);
		gameObject.SetActive (true);
	}

	public void DroppedInUpperActionArea() {
		SetNewState (ItemState.Dropped);
		gameObject.SetActive (false);
	}

	public bool PutItemInTempStore() {
		if (m_Inventory.PutItemInTempStore (this)) {
			if (m_LastState == ItemState.IdleOutside) {
				if(RemovedFromPickupArea != null) {
					RemovedFromPickupArea();
				}
			}
			SetNewState (ItemState.IdleInTempStore);
			return true;
		}
		return false;
	}

	public void RotateClockwise()
	{
		m_Rotation += 90f;
		if (m_Rotation >= 360f) {
			m_Rotation -= 360f;
		}
		Rotate ();
	}

	public void RotateCounterClockwise()
	{
		m_Rotation -= 90f;
		if (m_Rotation < 0f) {
			m_Rotation += 360f;
		}
		Rotate ();
	}

	private void Rotate() {
		AudioManager.Instance.SpawnAudio (AudioEvent.General_RotateItem, transform.position, transform.rotation);
		transform.rotation = Quaternion.Euler (0f, 0f, m_Rotation);
	}

	public void ReceivedByKnight(KnightController knight) {
		if (KnightReceiveBehaviour != null) {
			KnightReceiveBehaviour(knight);
		}
	}

	public void ReceivedBySquire(SquireController squire) {
		if (SquireReceiveBehaviour != null) {
			SquireReceiveBehaviour(squire);
		}
	}
}
