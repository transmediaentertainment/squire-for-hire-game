﻿using UnityEngine;
using System.Collections;

public class TwoSlotItem : Item {

	// Use this for initialization
	protected override void Awake ()
	{
		base.Awake ();
		m_SlotsFilled = new bool[2, 1];
		m_SlotsFilled [0, 0] = true;
		m_SlotsFilled [1, 0] = true;
	}

}
