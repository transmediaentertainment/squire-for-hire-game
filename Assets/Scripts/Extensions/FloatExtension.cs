﻿using UnityEngine;
using System.Collections;

public static class FloatExtension {
	public static bool IsApproximately(this float f, float other) {
		return (other <= f + 0.01f) && (other >= f - 0.01f);
	}
}
