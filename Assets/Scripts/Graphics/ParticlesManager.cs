﻿using UnityEngine;
using System.Collections;
using PathologicalGames;

public enum ParticleTemplate 
{
	EnemyGotHit,
	KnightGotHit
}

public class ParticlesManager : MonoSingleton<ParticlesManager> {

	public ParticleSystem m_EnemyGotHitPrefab;
	public ParticleSystem m_KnightGotHitPrefab;

	public GameObject SpawnParticles(ParticleTemplate template, Vector3 position, Quaternion? rotation = null) {
		var prefab = GetPrefab (template);

		var spawned = PoolManager.Pools ["Particles"].Spawn (prefab, position, (rotation != null) ? (Quaternion)rotation : prefab.transform.rotation);

		return spawned.gameObject;
	}

	private ParticleSystem GetPrefab(ParticleTemplate template) {
		switch (template) {
		case ParticleTemplate.EnemyGotHit:
			return m_EnemyGotHitPrefab;
		case ParticleTemplate.KnightGotHit:
			return m_KnightGotHitPrefab;
		default:
			return null;
		}
	}
}
