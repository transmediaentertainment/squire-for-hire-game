﻿using UnityEngine;

public static class MeshUtilities {

	public static Vector2[] GetUVsForPositionedQuad(int xPos, int yPos, int gridWidth, int gridHeight, Mesh mesh) {
		float uRatio = 1f / gridWidth;
		float vRatio = 1f / gridHeight;
		float left = uRatio * xPos;
		float right = uRatio * (xPos + 1);
		float bottom = vRatio * yPos;
		float top = vRatio * (yPos + 1);
		var uvs = mesh.uv;
		uvs[0] = new Vector2(left, bottom);
		uvs[1] = new Vector2(right, top);
		uvs[2] = new Vector2(right, bottom);
		uvs[3] = new Vector2(left, top);
		return uvs;
	}
	
}
