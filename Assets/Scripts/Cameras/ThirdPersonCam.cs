﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;

public class ThirdPersonCam : MonoBehaviour {

	public Vector3 m_DefaultCamRot = new Vector3(3f, 0f, 0f);
	public Vector3 m_DefaultPivotRot = new Vector3(0f, 8.5f, 0f);

	public Vector3 m_PivotOffset;

	private Transform m_SquireTransfrom;

	public Transform m_Pivot;

	private MouseLook m_MouseLook;

	public float m_SmoothSpeed = 5f;

	void Awake() {
		m_MouseLook = new MouseLook ();
		m_MouseLook.Init (m_Pivot, this.transform);
		m_MouseLook.smooth = true;
		m_MouseLook.smoothTime = m_SmoothSpeed;
		m_MouseLook.SnapRotation(m_Pivot, this.transform, this.m_DefaultPivotRot, this.m_DefaultCamRot);
	}

	public void Init(Transform squireTransform) {
		m_SquireTransfrom = squireTransform;

		SystemEventCenter.Instance.LateUpdateEvent += HandleLateUpdateEvent;
	}

	public void CleanUp() {
		SystemEventCenter.Instance.LateUpdateEvent -= HandleLateUpdateEvent;
	}

	private float m_Timer = 0f;
	public float m_WaitTime = 1f;

	private bool m_LookPressed;
	public void LookButtonPressed() {
		m_LookPressed = true;
	}

	void HandleLateUpdateEvent (float deltaTime)
	{
		m_Pivot.position = m_SquireTransfrom.position + m_PivotOffset;

		m_MouseLook.smoothTime = m_SmoothSpeed;

		if (m_LookPressed) {
			m_MouseLook.LookRotation (m_Pivot, this.transform);
			m_Timer = 0f;
		} else {
			m_Timer += deltaTime;
			if(m_Timer >= m_WaitTime) {
				m_MouseLook.SmoothDefaultRotataion(m_Pivot, this.transform, this.m_DefaultPivotRot, this.m_DefaultCamRot);
			} else {
				m_MouseLook.ContinueLookRotation(m_Pivot, this.transform);
			}
		}
		m_LookPressed = false;
	}
}
