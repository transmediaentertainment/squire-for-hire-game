﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class MovingUICameraController : MonoBehaviour {

	public Vector3 m_OnScreenPos;
	public Vector3 m_OffScreenPos;
	public float m_Duration;
	public Ease m_EaseType;

	private bool m_IsOn = false;

	private SquireController m_Squire;

	private KnightController m_Knight;

	public void Init(SquireController squire, KnightController knight) {
		m_Squire = squire;
		m_Knight = knight;

		SystemEventCenter.Instance.LateUpdateEvent += HandleLateUpdateEvent;
	}

	void HandleLateUpdateEvent (float obj)
	{
		if (m_IsOn) {
			if(!m_Squire.IsSquirePickingUp && m_Knight.State != KnightController.KnightState.WaitingForWeapon) {
				OffScreen();
			}
		} else {
			if(m_Squire.IsSquirePickingUp || m_Knight.State == KnightController.KnightState.WaitingForWeapon) {
				OnScreen();
			}
		}
	}

	public void CleanUp()
	{
		transform.position = m_OnScreenPos;
		m_IsOn = true;
		SystemEventCenter.Instance.LateUpdateEvent -= HandleLateUpdateEvent;
		m_Squire = null;
		m_Knight = null;
	}

	private void OffScreen() {
		DoMove (m_OffScreenPos);
		m_IsOn = false;
	}

	private void OnScreen() {
		DoMove (m_OnScreenPos);
		m_IsOn = true;
	}

	private void DoMove(Vector3 target) {
		transform.DOMove (target, m_Duration).SetEase (m_EaseType);
	}

	void Update() {
		if(Input.GetKeyDown(KeyCode.I)) {
			OnScreen();
		}

		if(Input.GetKeyDown(KeyCode.K)) {
			OffScreen();
		}
	}

}
