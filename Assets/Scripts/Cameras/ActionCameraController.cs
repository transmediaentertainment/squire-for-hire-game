﻿using UnityEngine;
using System.Collections;

public class ActionCameraController : MonoBehaviour {

	private Transform m_Knight;
	private Transform m_Squire;

	public Vector3 m_StartingOffset = new Vector3(1.5f, 3f, 0f);
	public float m_HeightRatio = 0.5f;

	public float m_Damping = 5f;

	public void Init(KnightController knight, SquireController squire) {
		m_Knight = knight.transform;
		m_Squire = squire.transform;
	}

	public void CleanUp() {
		m_Knight = null;
		m_Squire = null;
	}

	void Update() {
		if (m_Knight != null && m_Squire != null) {
			var midpoint = (m_Knight.position + m_Squire.position) * 0.5f;
			var dist = (m_Knight.position - m_Squire.position).magnitude;
			transform.LookAt(midpoint);
			transform.position = Vector3.Lerp(transform.position, m_Squire.position + m_StartingOffset + Vector3.up * dist * m_HeightRatio, Time.deltaTime * m_Damping);
		}
	}
}
