﻿using UnityEngine;
using System;

// This should be moved to the TransTech namespace in Unity 4
/// <summary>
/// Provides events for Unity Update and Fixed Update events
/// </summary>
public class SystemEventCenter : MonoSingleton<SystemEventCenter>
{
	/// <summary>
	/// Fired before all update events.
	/// </summary>
    public event Action<float> EarlyUpdateEvent;
	
	/// <summary>
	/// Fired after all early update events and before all late update events.
	/// </summary>
    public event Action<float> UpdateEvent;
	
	/// <summary>
	/// Fired after all update events.
	/// </summary>
    public event Action<float> LateUpdateEvent;
	
	/// <summary>
	/// Fired before all fixed update events.
	/// </summary>
    public event Action<float> EarlyFixedUpdateEvent;
	
	/// <summary>
	/// Fired after all early fixed update events and before all late fixed update events.
	/// </summary>
    public event Action<float> FixedUpdateEvent;
	
	/// <summary>
	/// Fired after all fixed update events.
	/// </summary>
    public event Action<float> LateFixedUpdateEvent;
	
	/// <summary>
	/// The delta time.
	/// </summary>
    private float m_DeltaTime;
	/// <summary>
	/// The fixed delta time.
	/// </summary>
    private float m_FixedDeltaTime;

	/// <summary>
	/// Called when Unity updates, calls early update then regular update
	/// </summary>
    private void Update()
    {
        m_DeltaTime = Time.deltaTime;
        if (EarlyUpdateEvent != null)
            EarlyUpdateEvent(m_DeltaTime);

        if (UpdateEvent != null)
            UpdateEvent(m_DeltaTime);
    }
	
	/// <summary>
	/// Called when Unity late updates, calls late update
	/// </summary>
    private void LateUpdate()
    {
        if (LateUpdateEvent != null)
            LateUpdateEvent(m_DeltaTime);
    }
	
	/// <summary>
	/// Called when Unity fixed updates, calls early fixed update then regular fixed update then later fixed update
	/// </summary>
    private void FixedUpdate()
    {
        m_FixedDeltaTime = Time.fixedDeltaTime;

        if (EarlyFixedUpdateEvent != null)
            EarlyFixedUpdateEvent(m_FixedDeltaTime);

        if (FixedUpdateEvent != null)
            FixedUpdateEvent(m_FixedDeltaTime);

        if (LateFixedUpdateEvent != null)
            LateFixedUpdateEvent(m_FixedDeltaTime);
    }
}